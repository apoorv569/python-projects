import pygame
from Modules.display import Display

class Event:
    def __init__(self):
        self.width, self.height = 700, 700
        self.win = pygame.display.set_mode((self.width, self.height))
        self.fps = 60
        self.clock = pygame.time.Clock()
        self.run = True
        self.display = Display()
#        self.sound = Sound()
#        self.color = Color()
#        self.font = Font(self.color)
#        self.button = Button(self.sound, self.color, self.font, self.width, self.height)
#        self.game = Game(self.sound, self.color, self.font, self.width, self.height)

    def main_game(self):
        while self.run:
            self.clock.tick(self.fps)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.run = False

                if event.type == pygame.MOUSEMOTION:
                    pygame.draw.rect(self.win, (0, 0, 0), (0, 0, self.width, 100))

                    mx = event.pos[0]
                    self.display.piece(self.win, mx)

                if event.type == pygame.MOUSEBUTTONDOWN:

                    self.display.game(self.win, self.display.zeros_grid())

#                pygame.display.update()

            self.display.board(self.win, self.display.zeros_grid())
            pygame.display.update()

        pygame.quit()

Event().main_game()
