import pygame
import numpy as np

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 200)
RED = (200, 0, 0)
YELLOW = (200, 200, 0)

class Display:
    def __init__(self):
        self.players = ['player1', 'player2']
        self.turn = 0
        self.square_size = 100
        self.radius = 45
        self.row_count = 6
        self.column_count = 7

    def zeros_grid(self):
        grid = np.zeros((self.row_count, self.column_count))
        #grid = [[0 for _ in range(700)] for _ in range(700)]
        return grid

#grid_index_x_clicked = int((mouse_x - grid_start_x) / grid_item_width)

#grid = [[0 for _ in range(width)] for _ in range(height)]

    def board(self, win, grid):
#        grid = self.zeros_grid()

        for r in range(self.row_count):
            for c in range(self.column_count):
                pygame.draw.rect(win, BLUE,
                                 (c * self.square_size, r * self.square_size + self.square_size,
                                  self.square_size, self.square_size))
#                if grid[r][c] == 1:
#                if self.turn == 0:
#                    pygame.draw.circle(win, RED,
#                                       (c * self.square_size + self.square_size//2,
#                                        700 - (r * self.square_size + self.square_size//2)),
#                                       self.radius)
#                elif grid[r][c] == 2:
#                elif self.turn == 1:
#                else:
#                    pygame.draw.circle(win, YELLOW,
#                                       (c * self.square_size + self.square_size//2,
#                                        700 - (r * self.square_size + self.square_size//2)),
#                                       self.radius)
#                else:
                pygame.draw.circle(win, BLACK,
                                   (c * self.square_size + self.square_size//2,
                                    r * self.square_size + self.square_size + self.square_size//2),
                                   self.radius)

        for r in range(self.row_count):
            for c in range(self.column_count):
                if grid[r][c] == 1:
#                if self.turn == 0:
                    pygame.draw.circle(win, RED,
                                       (c * self.square_size + self.square_size//2,
                                        700 - (r * self.square_size + self.square_size//2)),
                                       self.radius)
                elif grid[r][c] == 2:
#                elif self.turn == 1:
#                else:
                    pygame.draw.circle(win, YELLOW,
                                       (c * self.square_size + self.square_size//2,
                                        700 - (r * self.square_size + self.square_size//2)),
                                       self.radius)

#    def drop_piece(self, win):
#        mouse_pos = pygame.mouse.get_pos()
#        row = mouse_pos[1]//self.square_size
#        column = mouse_pos[0]//self.square_size

#        for r in range(self.row_count):
#            for c in range(self.column_count):
#        if self.turn == 0:
#            pygame.draw.circle(win, RED,
#                               (column * self.square_size + self.square_size//2,
#                                row * self.square_size + self.square_size//2),
#                               self.radius)
#        elif self.turn == 1:
#                else:
#            pygame.draw.circle(win, YELLOW,
#                               (column * self.square_size + self.square_size,
#                                row * self.square_size),
#                               self.radius)
#        pygame.display.update()

    def is_valid_move(self, grid, column):
#        grid = self.zeros_grid()
        return grid[self.row_count-1][column] == 0

    def get_next_open_row(self, grid, column):
        for r in range(self.row_count):
            if grid[r][column] == 0:
                return r

    def piece(self, win, mx):
        if self.turn == 0:
            pygame.draw.circle(win, RED, (mx, self.square_size//2), self.radius)
        elif self.turn == 1:
            pygame.draw.circle(win, YELLOW, (mx, self.square_size//2), self.radius)

#    def drop_piece(self, win, mouse_pos):
#        mouse_pos = pygame.mouse.get_pos()

#        for r in range(self.row_count):
#            for c in range(self.column_count):
#                if self.turn == self.players[0]:
#                    pygame.draw.circle(win, RED, ((mouse_pos[c])//800, (mouse_pos[r])//600), self.radius)
#                elif self.turn == self.players[1]:
#                    pygame.draw.circle(win, YELLOW, (mouse_pos[0], mouse_pos[1]), self.radius)

#        pygame.display.update()

    def drop_piece(self, grid, row, column, piece):
#        grid = self.zeros_grid()
        grid[row][column] = piece

    def game(self, win, grid):
        mouse_pos = pygame.mouse.get_pos()
        column = mouse_pos[0]//self.square_size
#        row = self.get_next_open_row(grid, column)

        if self.turn == 0:
#            mx, my = pygame.mouse.get_pos()
#            grid = self.zeros_grid()
#            column = mx//self.square_size

            print(mouse_pos)
#            print(np.flip(grid, 0))
            print(column)

            if self.is_valid_move(grid, column):
                row = self.get_next_open_row(grid, column)
                self.drop_piece(grid, row, column, 1)
#                self.drop_piece(win)

#            print(np.flip(grid, 0))

        elif self.turn == 1:
#            mx, my = pygame.mouse.get_pos()
#            grid = self.zeros_grid()
#            column = mx//self.square_size

            print(mouse_pos)
#            print(np.flip(grid, 0))
            print(column)

            if self.is_valid_move(grid, column):
                row = self.get_next_open_row(grid, column)
                self.drop_piece(grid, row, column, 2)
#                self.drop_piece(win)

#            print(np.flip(grid, 0))

        self.turn += 1
        self.turn = self.turn % 2
#        print(np.flip(grid, 0))

#    def draw(self):
#        print(np.flip(self.zeros_grid(), 0))
