import pygame
from Modules.grid import Grid
from Modules.constants import WIDTH, HEIGHT, ROWS, COLUMNS, BOX_SIZE
from Modules.game import Game

FPS = 60

WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('Tic Tac Toe')

# Main event loop.
def main():
    run = True
    clock = pygame.time.Clock()
    player = "X"

    grid = Grid()
    game = Game()

    grid.grid_list()
#    game.moves(player)

    while run:
        clock.tick(FPS)

 #       game.moves(player)

       # game.is_free()
       # game.tie_game()
       # game.has_won()
       # game.has_lost()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

            if event.type == pygame.MOUSEBUTTONDOWN:

                # Setting mouse position
                row, column = pygame.mouse.get_pos()
            #    print(row, column)

                if event.button == 1:
                    turn = grid.assign_turn(row // BOX_SIZE, column // BOX_SIZE, player)

#                    if not game.is_full():
                    if game.has_won:
                        print(game.has_won)
                     #   elif game.has_lost:
                     #       print("You lose")
                     #   elif game.tie_game:
                     #       print("Tie game!")
                    else:
                        print("Error!")

                    # Swtching turn
                    if turn:
                        if player == "X":
                            player = "O"
                        else:
                            player = "X"

#                    if grid.grid[0][0] == player and grid.grid[1][0] == player and grid.grid[2][0] == player or \
#                       grid.grid[0][1] == player and grid.grid[1][1] == player and grid.grid[2][1] == player or \
#                       grid.grid[0][2] == player and grid.grid[1][2] == player and grid.grid[2][2] == player or \
#                       grid.grid[0][0] == player and grid.grid[0][1] == player and grid.grid[0][2] == player or \
#                       grid.grid[1][0] == player and grid.grid[1][1] == player and grid.grid[1][2] == player or \
#                       grid.grid[2][0] == player and grid.grid[2][1] == player and grid.grid[2][2] == player or \
#                       grid.grid[0][0] == player and grid.grid[1][1] == player and grid.grid[2][2] == player or \
#                       grid.grid[0][2] == player and grid.grid[1][1] == player and grid.grid[2][0] == player:
#                        print(f"{player} won!")
#                        break
#                    else:
#                        print("Error!")

                    grid.draw_image_on_click(WIN)
                 #   print(WIN, row, column)

        grid.draw(WIN)
        game.draw()
        pygame.display.update()

    pygame.quit()

main()
