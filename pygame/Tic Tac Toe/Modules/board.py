import pygame
from .constants import WHITE, HEIGHT, WIDTH, ROWS, COLUMNS, BOX_SIZE, NOUGHTS, CROSSES, PADDING

class Board:
    def __init__(self):
        self.grid = []
        self.vertical_lines = []
        self.horizontol_lines = []
        self.x = 0
        self.y = 0
        self.x_pos = 0
        self.y_pos = 0
#        self.calculate_postion()

    def grid(self):
        for row in range(ROWS):
            coordinates = []
            for column in range(COLUMNS):
                coordinates.append((row, column))
            self.grid.append(coordinates)

    def get_mouse_position(self):
        self.x, self.y = pygame.mouse.get_pos()
        self.x_pos = self.x // BOX_SIZE
        self.y_pos = self.y // BOX_SIZE
        return self.x_pos, self.y_pos

    def calculate_postion(self):
        self.x = BOX_SIZE * self.x_pos + BOX_SIZE // 2
        self.y = BOX_SIZE * self.y_pos + BOX_SIZE // 2
        return self.x, self.y

    def move(self, row, column):
        self.grid[self.x_pos][self.y_pos], self.grid[row][column] = self.grid[row][column], self.grid[self.x_pos][self.y_pos]

    def draw_vertical_lines(self, win):
        for line in range(ROWS):
            step = (line + 1) * BOX_SIZE
            self.vertical_lines.append(step)

            pygame.draw.line(win, WHITE, (step, 0), (step, HEIGHT), 2)

    def draw_horizontol_lines(self, win):
        for line in range(COLUMNS):
            step = (line + 1) * BOX_SIZE
            self.horizontol_lines.append(step)

            pygame.draw.line(win, WHITE, (0, step), (WIDTH, step), 2)

    def draw(self, win):
        self.draw_vertical_lines(win)
        self.draw_horizontol_lines(win)
        win.blit(NOUGHTS, (self.x, self.y))
