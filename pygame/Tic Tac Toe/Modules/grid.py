import pygame
from .constants import BLACK, WHITE, HEIGHT, WIDTH, ROWS, COLUMNS, BOX_SIZE, NOUGHTS, CROSSES, PADDING

class Grid:
    def __init__(self):
        self.grid = []
        self.vertical_lines = []
        self.horizontol_lines = []
     #   self.valid_moves = False
     #   self.image = "X"
        self.width = 200
        self.height = 200
        self.x = 0
        self.y = 0
#        self.mx = 0
#        self.my = 0
#        self.mx_pos = 0
#        self.my_pos = 0

    #grid_list = [[0 for x in range(3)] for y in range(3)]

    # Making a tuples of row and columns
   # def grid_list(self):
   #     for row in range(ROWS):
   #         coordinates = []
   #         for column in range(COLUMNS):
   #             coordinates.append((row, column))
   #         self.grid.append(coordinates)
   #     print(self.grid)

    def grid_list(self):
        for row in range(ROWS):
            coordinates = []
            for column in range(COLUMNS):
                coordinates.append(None)
            self.grid.append(coordinates)
        print(self.grid)

#    def draw_box(self, win):
#        for self.x in range(0, 401, 200):
#            for self.y in range(0, 401, 200):
#                if self.x <= 401 and self.y <= 401:
#                    self.x += 0
#                    self.y += 0
#                  #  self.grid.append((pos))
#                    print(self.x, self.y)
#                    pygame.draw.rect(win, WHITE, (self.x, self.y, self.width, self.height))
#                    win.blit(CROSSES, (self.x, self.y))
#                else:
#                    break
#       # print(self.x, self.y)

    # Draw the grid
    def draw_grid(self, win):
        pygame.draw.rect(win, WHITE, (0, 0, self.width, self.height))
        pygame.draw.rect(win, WHITE, (200, 0, self.width, self.height))
        pygame.draw.rect(win, WHITE, (400, 0, self.width, self.height))
        pygame.draw.rect(win, WHITE, (0, 200, self.width, self.height))
        pygame.draw.rect(win, WHITE, (200, 200 , self.width, self.height))
        pygame.draw.rect(win, WHITE, (400, 200, self.width, self.height))
        pygame.draw.rect(win, WHITE, (0, 400, self.width, self.height))
        pygame.draw.rect(win, WHITE, (200, 400, self.width, self.height))
        pygame.draw.rect(win, WHITE, (400, 400, self.width, self.height))

    # Draw vertical lines
    def draw_vertical_lines(self, win):
        for line in range(ROWS):
            step = (line + 1) * BOX_SIZE

            pygame.draw.line(win, BLACK, (step, 0), (step, HEIGHT), 2)

    # Draw horizontal lines
    def draw_horizontol_lines(self, win):
        for line in range(COLUMNS):
            step = (line + 1) * BOX_SIZE

            pygame.draw.line(win, BLACK, (0, step), (WIDTH, step), 2)

    # Draw image where the user clicks with mouse
    def draw_image_on_click(self, win):
        for y in range(len(self.grid)):
            for x in range(len(self.grid)):
                if self.get_box_value(x, y) == "X":
                    win.blit(CROSSES, (x*BOX_SIZE, y*BOX_SIZE))
                elif self.get_box_value(x, y) == "O":
                    win.blit(NOUGHTS, (x*BOX_SIZE, y*BOX_SIZE))

    # Get the coordinates for where the user clicks
    def get_box_value(self, x, y):
        return self.grid[y][x]

    # Set the value for the returned coordinated
    def set_box_value(self, x, y, value):
        self.grid[y][x] = value

  #  def get_mouse_position(self):
  #      self.mx, self.my = pygame.mouse.get_pos()
  #      self.mx_pos = self.mx // BOX_SIZE
  #      self.my_pos = self.my // BOX_SIZE
  #      return self.mx_pos, self.my_pos

    # Assigning player turn
    def assign_turn(self, x, y, move):
        # Invalid move
        if self.grid[y][x] is not None:
            return False

        # Atribute box value
        self.set_box_value(x, y, move)
        return True

    #    if any([self.get_box_value(x, y) == item for item in self.grid]):
    #    if self.get_box_value(x, y) == self.grid[y][x]:
    #        if move == "X":
    #            self.set_box_value(x, y, "X")
    #        elif move == "O":
    #            self.set_box_value(x, y, "O")

#    def moves(self):
    # [['X', 'O', 'X'],
    #  ['O', 'X', 'X'],
    #  ['O', 'O', 'X']]
#        if self.grid[0][0] == self.image and self.grid[1][0] == self.image and self.grid[2][0] == self.image or \
#           self.grid[0][1] == self.image and self.grid[1][1] == self.image and self.grid[2][1] == self.image or \
#           self.grid[0][2] == self.image and self.grid[1][2] == self.image and self.grid[2][2] == self.image or \
#           self.grid[0][0] == self.image and self.grid[0][1] == self.image and self.grid[0][2] == self.image or \
#           self.grid[1][0] == self.image and self.grid[1][1] == self.image and self.grid[1][2] == self.image or \
#           self.grid[2][0] == self.image and self.grid[2][1] == self.image and self.grid[2][2] == self.image or \
#           self.grid[0][0] == self.image and self.grid[1][1] == self.image and self.grid[2][2] == self.image or \
#           self.grid[0][2] == self.image and self.grid[1][1] == self.image and self.grid[2][0] == self.image:
#            return True
#        else:
#            return False

    # Draw everything defined in this class
    def draw(self, win):
        self.draw_grid(win)
        self.draw_horizontol_lines(win)
        self.draw_vertical_lines(win)
        self.draw_image_on_click(win)
#        print(self.grid)
