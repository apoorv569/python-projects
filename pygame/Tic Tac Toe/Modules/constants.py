import pygame

# Screen elements
WIDTH, HEIGHT = 600, 600
ROWS, COLUMNS = 3, 3
BOX_SIZE = WIDTH//COLUMNS

PADDING = 20

# Colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

# Assets
NOUGHTS = pygame.transform.scale(pygame.image.load('Assets/Noughts.png'), (200, 200))
CROSSES = pygame.transform.scale(pygame.image.load('Assets/Crosses.png'), (200, 200))
