import pygame
from .constants import NOUGHTS, CROSSES
from .grid import Grid

grid = Grid()
#grid.moves()

class Game:
    def __init__(self):
        self.image = [NOUGHTS, CROSSES]
    #    self.move = grid.grid
    #    self.valid_moves = []
        self.is_free = False
        self.is_winner = False
        self.is_tie = False
    #    self.choice = None

        grid.grid_list()

    # Defining valid moves
    def moves(self, player):
       # [['X', 'O', 'X'],
       #  ['O', 'X', 'X'],
       #  ['O', 'O', 'X']]
    #    self.valid_moves = (grid.grid[0][0] == self.image and grid.grid[1][0] == self.image and grid.grid[2][0] == self.image) or \
    #           (grid.grid[0][1] == self.image and grid.grid[1][1] == self.image and grid.grid[2][1] == self.image) or \
    #           (grid.grid[0][2] == self.image and grid.grid[1][2] == self.image and grid.grid[2][2] == self.image) or \
    #           (grid.grid[0][0] == self.image and grid.grid[0][1] == self.image and grid.grid[0][2] == self.image) or \
    #           (grid.grid[1][0] == self.image and grid.grid[1][1] == self.image and grid.grid[1][2] == self.image) or \
    #           (grid.grid[2][0] == self.image and grid.grid[2][1] == self.image and grid.grid[2][2] == self.image) or \
    #           (grid.grid[0][0] == self.image and grid.grid[1][1] == self.image and grid.grid[2][2] == self.image) or \
    #           (grid.grid[0][2] == self.image and grid.grid[1][1] == self.image and grid.grid[2][0] == self.image)
    #    return self.valid_moves
        if grid.grid[0][0] == player and grid.grid[1][0] == player and grid.grid[2][0] == player or \
           grid.grid[0][1] == player and grid.grid[1][1] == player and grid.grid[2][1] == player or \
           grid.grid[0][2] == player and grid.grid[1][2] == player and grid.grid[2][2] == player or \
           grid.grid[0][0] == player and grid.grid[0][1] == player and grid.grid[0][2] == player or \
           grid.grid[1][0] == player and grid.grid[1][1] == player and grid.grid[1][2] == player or \
           grid.grid[2][0] == player and grid.grid[2][1] == player and grid.grid[2][2] == player or \
           grid.grid[0][0] == player and grid.grid[1][1] == player and grid.grid[2][2] == player or \
           grid.grid[0][2] == player and grid.grid[1][1] == player and grid.grid[2][0] == player:
            return True
        else:
            return False

    # Checking if grid is full
    def is_full(self):
    #    if grid.grid[y][x] is not None:
    #        self.is_free = False
    #    else:
    #        self.is_free = True
        for row in grid.grid:
            if None in row:
                return False # at least one empty position
        return True # no None found, so no empty positions, so the board is full


    # Checking if someone has won
    def has_won(self):
        if self.moves:
            self.is_winner = True

    # Checking if someone has lost
    def has_lost(self):
        if not self.moves:
            self.is_winner = False

    # Checking if the game is a tie
    def tie_game(self):
        if self.is_winner is False and self.is_full is True:
            self.is_tie = True

    def draw(self):
        pass
#        print(self.moves)
#        print(grid.moves)
