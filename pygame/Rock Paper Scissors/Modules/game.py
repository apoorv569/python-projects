import random
import pygame
from .constants import SCORE_FONT

class Game:
    """
    Defining game logics
    """
    def __init__(self, sound, color, font, width, height):
        self.font = font
        self.sound = sound
        self.color = color
        self.width = width
        self.height = height
        self.choices = ['rock', 'paper', 'scissor']
        self.players = ["Player 1", "Player 2"]
        self.p1_score = 0
        self.p2_score = 0
        self.tie_count = 0
        self.round_count = 1

    def detect_mouse(self, choice):
        """
        Detect mouse position
        """
        mx, my = pygame.mouse.get_pos()
        if mx > 20 and mx < 260 and my > 500 and my < 550:
            choice = self.choices[0]
        elif mx > 280 and mx < 520 and my > 500 and my < 550:
            choice = self.choices[1]
        elif mx > 540 and mx < 780 and my > 500 and my < 550:
            choice = self.choices[2]
        else:
            print("No Choice made.")

        return choice

    def p1_choice_text(self, win, choice):
        """
        Defining where to draw to player 1 choice text
        """
        pygame.time.delay(1000)
        win.blit(self.font.draw_text(choice),
                 (20, self.height/2 - self.font.draw_text(choice).get_height()/2))
        pygame.display.update()
        pygame.time.delay(1000)

    def p2_choice_text(self, win, choice):
        """
        Defining where to draw to player 2 choice text
        """
        pygame.time.delay(1000)
        win.blit(self.font.draw_text(choice),
                 (self.width - self.font.draw_text(choice).get_width() - 20,
                  self.height/2 - self.font.draw_text(choice).get_height()/2))
        pygame.display.update()
        pygame.time.delay(1000)

    def result(self, win, result, sound):
        """
        Defining where to draw game result text
        """
        pygame.time.delay(1000)
        win.fill(self.color.set_color("background"))
        self.sound.game_sound(sound)
        win.blit(self.font.draw_text(result),
                 (self.width/2 - self.font.draw_text(result).get_width()/2,
                  self.height/2 - self.font.draw_text(result).get_height()/2))
        pygame.display.update()
        pygame.time.delay(3000)

    def logic(self, win):
        """
        Checking who chose what, and who wins
        """
        turn = self.players[0]
        if turn == self.players[0]:
            self.players[0] = self.detect_mouse(self.choices)
        turn = self.players[1]
        if turn == self.players[1]:
            self.players[1] = random.choice(self.choices)
            print(f"Player 2 chose - {self.players[1]}")

        if self.players[0] == self.choices[0] and self.players[1] == self.choices[0]:
            self.p1_choice_text(win, "rock_button_text")
            self.p2_choice_text(win, "rock_button_text")
            self.result(win, "tie_game_text", self.sound.tie_game_sound)
            self.tie_count += 1
            self.round_count += 1
            print("Tie game!")
        elif self.players[0] == self.choices[0] and self.players[1] == self.choices[1]:
            self.p1_choice_text(win, "rock_button_text")
            self.p2_choice_text(win, "paper_button_text")
            self.result(win, "looser_text", self.sound.looser_sound)
            self.p2_score += 1
            self.round_count += 1
            print("Player 2 wins!\nYou lose!")
        elif self.players[0] == self.choices[0] and self.players[1] == self.choices[2]:
            self.p1_choice_text(win, "rock_button_text")
            self.p2_choice_text(win, "scissors_button_text")
            self.result(win, "winner_text", self.sound.winner_sound)
            self.p1_score += 1
            self.round_count += 1
            print(self.p1_score)
            print("You win!")
        elif self.players[0] == self.choices[1] and self.players[1] == self.choices[0]:
            self.p1_choice_text(win, "paper_button_text")
            self.p2_choice_text(win, "rock_button_text")
            self.result(win, "winner_text", self.sound.winner_sound)
            self.p1_score += 1
            self.round_count += 1
            print("You win!")
        elif self.players[0] == self.choices[1] and self.players[1] == self.choices[1]:
            self.p1_choice_text(win, "paper_button_text")
            self.p2_choice_text(win, "paper_button_text")
            self.result(win, "tie_game_text", self.sound.tie_game_sound)
            self.tie_count += 1
            self.round_count += 1
            print("Tie game!")
        elif self.players[0] == self.choices[1] and self.players[1] == self.choices[2]:
            self.p1_choice_text(win, "paper_button_text")
            self.p2_choice_text(win, "scissors_button_text")
            self.result(win, "looser_text", self.sound.looser_sound)
            self.p2_score += 1
            self.round_count += 1
            print("Player 2 wins!\nYou lose!")
        elif self.players[0] == self.choices[2] and self.players[1] == self.choices[0]:
            self.p1_choice_text(win, "scissors_button_text")
            self.p2_choice_text(win, "rock_button_text")
            self.result(win, "looser_text", self.sound.looser_sound)
            self.round_count += 1
            self.p2_score += 1
            print("Player 2 wins!\nYou lose!")
        elif self.players[0] == self.choices[2] and self.players[1] == self.choices[1]:
            self.p1_choice_text(win, "scissors_button_text")
            self.p2_choice_text(win, "paper_button_text")
            self.result(win, "winner_text", self.sound.winner_sound)
            self.p1_score += 1
            self.round_count += 1
            print("You win!")
        elif self.players[0] == self.choices[2] and self.players[1] == self.choices[2]:
            self.p1_choice_text(win, "scissors_button_text")
            self.p2_choice_text(win, "scissors_button_text")
            self.result(win, "tie_game_text", self.sound.tie_game_sound)
            self.tie_count += 1
            self.round_count += 1
            print("Tie game!")
        else:
            print("Please select a valid option.")

        return self.p1_score, self.p2_score

#    def track_counter(self, win, text, x, y):
#        """
#        Keep track of counters like player 1 score,
#        player 2 score, ties and round count.
#        """
#        count = SCORE_FONT.render(text, 1, self.color.set_color("text"))
#        win.blit(count, (x, y))

    def player1_score(self, win):
        """
        Keep track of player 1 score
        """
        p1_score_text = SCORE_FONT.render(f"Player 1: {self.p1_score}",
                                          1, self.color.set_color("text"))
        win.blit(p1_score_text, (20, 40))
    def player2_score(self, win):
        """
        Keep track of player 2 score
        """
        p2_score_text = SCORE_FONT.render(f"Player 2: {self.p2_score}",
                                          1, self.color.set_color("text"))
        win.blit(p2_score_text,
                 (self.width - p2_score_text.get_width() - 20, 40))
    def tie_count_check(self, win):
        """
        Keep track of tie count
        """
        tie_count_text = SCORE_FONT.render(f"Ties: {self.tie_count}",
                                           1, self.color.set_color("text"))
        win.blit(tie_count_text,
                 (self.width/2 - tie_count_text.get_width()/2, 40))
    def round_count_check(self, win):
        """
        Keep track of round
        """
        round_count_text = SCORE_FONT.render(f"Round: {self.round_count}",
                                             1, self.color.set_color("text"))
        win.blit(round_count_text,
                 (self.width/2 - round_count_text.get_width()/2, 10))

    def draw(self, win):
        """
        Draw counters on screen
        """
#        self.track_counter(win, f"Player 1: {self.p1_score}", 20, 40)
#        self.track_counter(win, f"Player 2: {self.p2_score}", self.width - self.font.draw_text("player2_score_text").get_width() - 20, 40)
#        self.track_counter(win, f"Ties: {self.tie_count}", self.width/2 - self.font.draw_text("tie_game_text").get_width()/2, 40)
#        self.track_counter(win, f"Round: {self.round_count}", self.width/2 - self.font.draw_text("round_count_text").get_width()/2, 10)
        self.player1_score(win)
        self.player2_score(win)
        self.tie_count_check(win)
        self.round_count_check(win)