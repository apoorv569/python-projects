import pygame

pygame.font.init()

class Font:
    """
    Defining font and text database
    """
    def get_font(self, wanted_fonts):
        for font_name in wanted_fonts:
            print(f"Font name [{font_name}] exists... ", end='')
            if pygame.font.match_font(font_name) is not None:
                print(f"Font found!!")
                return font_name
            print("No")
        print("No font matched, defaulting to Pygame's default font")
        return None

    def __init__(self, color):

        self.color = color

        self.round_count = 1
        self.tie_count = 0

        fonts_priority = ["mononokinerdfontmono", "ubuntunerdfont", "comicsansms"]

        self.text_database = {
            "main_menu_title_text": {
                "text": "Rock Paper Scissors",
                "config": {"size": 45, "font": fonts_priority}
            },
            "settings_menu_title_text": {
                "text": "Settings",
                "config": {"size": 45, "font": fonts_priority}
            },
            "theme_menu_title_text": {
                "text": "Select a theme",
                "config": {"size": 45, "font": fonts_priority}
            },
            "accent_color_menu_title_text": {
                "text": "Select a accent color",
                "config": {"size": 45, "font": fonts_priority}
            },
            "winner_text": {
                "text": "You win!",
                "config": {"size": 60, "font": fonts_priority}
            },
            "looser_text": {
                "text": "You loose!",
                "config": {"size": 60, "font": fonts_priority}
            },
            "tie_game_text": {
                "text": "Tie game!",
                "config": {"size": 60, "font": fonts_priority}
            },
            "go_back_button_text": {
                "text": "<- Back",
                "config": {"size": 15, "font": fonts_priority}
            },
            "rock_button_text": {
                "text": "Rock",
                "config": {"size": 30, "font": fonts_priority}
            },
            "paper_button_text": {
                "text": "Paper",
                "config": {"size": 30, "font": fonts_priority}
            },
            "scissors_button_text": {
                "text": "Scissors",
                "config": {"size": 30, "font": fonts_priority}
            },
            "round_count_text": {
                "text": "Round: ",
                "config": {"size": 20, "font": fonts_priority}
            },
            "player1_score_text": {
                "text": "Player 1: ",
                "config": {"size": 20, "font": fonts_priority}
            },
            "player2_score_text": {
                "text": "Player 2: ",
                "config": {"size": 20, "font": fonts_priority}
            },
            "tie_count_text": {
                "text": "Ties: ",
                "config": {"size": 20, "font": fonts_priority}
            },
            "play_button_text": {
                "text": "Play",
                "config": {"size": 30, "font": fonts_priority}
            },
            "settings_button_text": {
                "text": "Settings",
                "config": {"size": 30, "font": fonts_priority}
            },
            "about_button_text": {
                "text": "About",
                "config": {"size": 30, "font": fonts_priority}
            },
            "quit_button_text": {
                "text": "Quit",
                "config": {"size": 30, "font": fonts_priority}
            },
            "sound_button_on_text": {
                "text": "Sound ON",
                "config": {"size": 30, "font": fonts_priority}
            },
            "sound_button_off_text": {
                "text": "Sound OFF",
                "config": {"size": 30, "font": fonts_priority}
            },
            "accent_color_button_text": {
                "text": "Accent color",
                "config": {"size": 30, "font": fonts_priority}
            },
            "theme_change_button_text": {
                "text": "Change theme",
                "config": {"size": 30, "font": fonts_priority}
            },
            "blue_accent_button_text": {
                "text": "Blue",
                "config": {"size": 30, "font": fonts_priority}
            },
            "green_accent_button_text": {
                "text": "Green",
                "config": {"size": 30, "font": fonts_priority}
            },
            "red_accent_button_text": {
                "text": "Red",
                "config": {"size": 30, "font": fonts_priority}
            },
            "purple_accent_button_text": {
                "text": "Purple",
                "config": {"size": 30, "font": fonts_priority}
            },
            "orange_accent_button_text": {
                "text": "Orange",
                "config": {"size": 30, "font": fonts_priority}
            },
            "pink_accent_button_text": {
                "text": "Pink",
                "config": {"size": 30, "font": fonts_priority}
            },
            "grey_accent_button_text": {
                "text": "Grey (Default)",
                "config": {"size": 30, "font": fonts_priority}
            },
            "yellow_accent_button_text": {
                "text": "Yellow",
                "config": {"size": 30, "font": fonts_priority}
            },
            "magenta_accent_button_text": {
                "text": "Magenta",
                "config": {"size": 30, "font": fonts_priority}
            },
            "lime_accent_button_text": {
                "text": "Lime",
                "config": {"size": 30, "font": fonts_priority}
            },
            "voilet_accent_button_text": {
                "text": "Voilet",
                "config": {"size": 30, "font": fonts_priority}
            },
            "teal_accent_button_text": {
                "text": "Teal",
                "config": {"size": 30, "font": fonts_priority}
            },
            "dark_theme_button_text": {
                "text": "Dark",
                "config": {"size": 30, "font": fonts_priority}
            },
            "light_theme_button_text": {
                "text": "Light",
                "config": {"size": 30, "font": fonts_priority}
            }
        }

        self.update_draw_text()

    def draw_text(self, key):
        """
        Get texts with the key on the dict above,
        like draw_text["tie-text"]
        """
        return self.drawable_texts[key]

    def update_draw_text(self):
        """
        Update the drawn text
        """
        self.drawable_texts = {}

        for key in self.text_database.keys():
            text = self.text_database[key]["text"]
            config = self.text_database[key]["config"]
            font = pygame.font.SysFont(self.get_font(config["font"]), config["size"])
            self.drawable_texts[key] = font.render(text, 1, self.color.set_color("text"))

        # Game button list
        self.game_buttons = [self.draw_text("rock_button_text"),
                             self.draw_text("paper_button_text"),
                             self.draw_text("scissors_button_text")]

        # Menu button list
        self.menu_buttons = [self.draw_text("play_button_text"),
                             self.draw_text("settings_button_text"),
                             self.draw_text("about_button_text"),
                             self.draw_text("quit_button_text")]

        # Setting menu button list with sound on
        self.settings_menu_buttons_sound_on = [self.draw_text("sound_button_on_text"),
                                               self.draw_text("accent_color_button_text"),
                                               self.draw_text("theme_change_button_text")]

        # Setting menu button list with sound off
        self.settings_menu_buttons_sound_off = [self.draw_text("sound_button_off_text"),
                                                self.draw_text("accent_color_button_text"),
                                                self.draw_text("theme_change_button_text")]

        # Theme menu button list
        self.theme_menu_buttons = [self.draw_text("dark_theme_button_text"),
                                   self.draw_text("light_theme_button_text")]

        # Accent menu button list
        self.accent_color_menu_buttons = [self.draw_text("blue_accent_button_text"),
                                          self.draw_text("green_accent_button_text"),
                                          self.draw_text("red_accent_button_text"),
                                          self.draw_text("purple_accent_button_text"),
                                          self.draw_text("orange_accent_button_text"),
                                          self.draw_text("pink_accent_button_text"),
                                          self.draw_text("grey_accent_button_text"),
                                          self.draw_text("yellow_accent_button_text"),
                                          self.draw_text("magenta_accent_button_text"),
                                          self.draw_text("lime_accent_button_text"),
                                          self.draw_text("voilet_accent_button_text"),
                                          self.draw_text("teal_accent_button_text")]
