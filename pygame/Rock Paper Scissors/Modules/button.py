import pygame

class Button:
    """
    Defining how and where to draw buttons
    on different menus and screens
    """
    def __init__(self, sound, color, font, width, height):
        self.font = font
        self.sound = sound
        self.color = color
        self.width = width
        self.height = height
        self.options = ['play', 'quit']
        self.num_of_buttons = 3
        self.gap = 20
        self.num_of_gaps = self.num_of_buttons + 1
        self.button_width = (self.width - (self.gap * self.num_of_gaps)) / self.num_of_buttons
        self.button_height = 50
        self.x = 0
        self.y = 0

    def game_button(self, mouse_pos, win):
        """
        Defining where to draw game buttons
        """
        self.num_of_buttons = 3

        for i in range(self.num_of_buttons):
            self.x = self.gap + ((self.gap + self.button_width) * i)
            self.y = 500

            if (self.x + self.button_width > mouse_pos[0] > self.x) and \
               (self.y + self.button_height > mouse_pos[1] > self.y):

                self.sound.button_sound(self.sound.button_hover_sound)

                pygame.draw.rect(win, self.color.set_color("accent"), (self.x, 500, self.button_width, self.button_height))
                win.blit(self.font.game_buttons[i],
                         (self.x + (self.button_width/2 - self.font.game_buttons[i].get_width()/2),
                          self.y + (self.button_height/2 - self.font.game_buttons[i].get_height()/2)))
            else:
                pygame.draw.rect(win, self.color.set_color("button"), (self.x, 500, self.button_width, self.button_height))
                win.blit(self.font.game_buttons[i],
                         (self.x + (self.button_width/2 - self.font.game_buttons[i].get_width()/2),
                          self.y + (self.button_height/2 - self.font.game_buttons[i].get_height()/2)))

    def main_menu_button(self, mouse_pos, win):
        """
        Defining where to draw main menu buttons
        """
        self.num_of_buttons = 4

        for i in range(self.num_of_buttons):
            self.x = self.width/2 - self.button_width/2
            self.y = self.gap + (self.height/self.num_of_buttons + ((self.button_height + self.gap)*(i)))

            if (self.x + self.button_width > mouse_pos[0] > self.x) and \
               (self.y + self.button_height > mouse_pos[1] > self.y):

                self.sound.button_sound(self.sound.button_hover_sound)

                pygame.draw.rect(win,
                                 self.color.set_color("accent"),
                                 (self.x, self.y,
                                  self.button_width, self.button_height))
                win.blit(self.font.menu_buttons[i],
                         (self.x + (self.button_width/2 - self.font.menu_buttons[i].get_width()/2),
                          self.y + (self.button_height/2 - self.font.menu_buttons[i].get_height()/2)))
            else:
                pygame.draw.rect(win,
                                 self.color.set_color("button"),
                                 (self.x, self.y,
                                  self.button_width, self.button_height))
                win.blit(self.font.menu_buttons[i],
                         (self.x + (self.button_width/2 - self.font.menu_buttons[i].get_width()/2),
                          self.y + (self.button_height/2 - self.font.menu_buttons[i].get_height()/2)))

    def settings_menu_button(self, mouse_pos, win):
        """
        Defining where to draw setting menu buttons
        """
        self.num_of_buttons = 3

        for i in range(self.num_of_buttons):
            self.x = self.width/2 - self.button_width/2
            self.y = self.gap + (self.height/self.num_of_buttons + ((self.button_height + self.gap)*(i)))

            if self.sound.muted is True:
                if (self.x + self.button_width > mouse_pos[0] > self.x) and \
                   (self.y + self.button_height > mouse_pos[1] > self.y):
                    pygame.draw.rect(win,
                                     self.color.set_color("accent"),
                                     (self.x, self.y,
                                      self.button_width, self.button_height))
                    win.blit(self.font.settings_menu_buttons_sound_off[i],
                             (self.x + (self.button_width/2 - self.font.settings_menu_buttons_sound_off[i].get_width()/2),
                              self.y + (self.button_height/2 - self.font.settings_menu_buttons_sound_off[i].get_height()/2)))
                else:
                    pygame.draw.rect(win,
                                     self.color.set_color("button"),
                                     (self.x, self.y,
                                      self.button_width, self.button_height))
                    win.blit(self.font.settings_menu_buttons_sound_off[i],
                             (self.x + (self.button_width/2 - self.font.settings_menu_buttons_sound_off[i].get_width()/2),
                              self.y + (self.button_height/2 - self.font.settings_menu_buttons_sound_off[i].get_height()/2)))
            elif self.sound.muted is False:
                if (self.x + self.button_width > mouse_pos[0] > self.x) and \
                   (self.y + self.button_height > mouse_pos[1] > self.y):
                    pygame.draw.rect(win,
                                     self.color.set_color("accent"),
                                     (self.x, self.y,
                                      self.button_width, self.button_height))
                    win.blit(self.font.settings_menu_buttons_sound_on[i],
                             (self.x + (self.button_width/2 - self.font.settings_menu_buttons_sound_on[i].get_width()/2),
                              self.y + (self.button_height/2 - self.font.settings_menu_buttons_sound_on[i].get_height()/2)))
                else:
                    pygame.draw.rect(win,
                                     self.color.set_color("button"),
                                     (self.x, self.y,
                                      self.button_width, self.button_height))
                    win.blit(self.font.settings_menu_buttons_sound_on[i],
                             (self.x + (self.button_width/2 - self.font.settings_menu_buttons_sound_on[i].get_width()/2),
                              self.y + (self.button_height/2 - self.font.settings_menu_buttons_sound_on[i].get_height()/2)))

    def theme_menu_button(self, mouse_pos, win):
        """
        Defining where to draw theme menu buttons
        """
        self.num_of_buttons = 2

        for i in range(self.num_of_buttons):
            self.x = self.width/2 - self.button_width/2
            self.y = self.gap + (self.height/self.num_of_buttons - ((self.button_height + self.gap)*(i)))

            if (self.x + self.button_width > mouse_pos[0] > self.x) and \
               (self.y + self.button_height > mouse_pos[1] > self.y):

                self.sound.button_sound(self.sound.button_hover_sound)

                pygame.draw.rect(win,
                                 self.color.set_color("accent"),
                                 (self.x, self.y,
                                  self.button_width, self.button_height))
                win.blit(self.font.theme_menu_buttons[i],
                         (self.x + (self.button_width/2 - self.font.theme_menu_buttons[i].get_width()/2),
                          self.y + (self.button_height/2 - self.font.theme_menu_buttons[i].get_height()/2)))
            else:
                pygame.draw.rect(win,
                                 self.color.set_color("button"),
                                 (self.x, self.y,
                                  self.button_width, self.button_height))
                win.blit(self.font.theme_menu_buttons[i],
                         (self.x + (self.button_width/2 - self.font.theme_menu_buttons[i].get_width()/2),
                          self.y + (self.button_height/2 - self.font.theme_menu_buttons[i].get_height()/2)))

    def accent_color_menu_button(self, mouse_pos, win):
        """
        Defining where to draw accent color menu buttons
        """
        self.num_of_buttons = 6

        for i in range(self.num_of_buttons):
            for j in range(2):
                self.x = (self.width/2 - self.button_width) + ((self.gap + self.button_width) * j)
                self.y = self.gap + (self.height/self.num_of_buttons + ((self.button_height + self.gap)*(i)))

                if (self.x + self.button_width > mouse_pos[0] > self.x) and \
                   (self.y + self.button_height > mouse_pos[1] > self.y):

                    self.sound.button_sound(self.sound.button_hover_sound)

                    pygame.draw.rect(win,
                                     self.color.set_color("accent"),
                                     (self.x, self.y,
                                      self.button_width, self.button_height))
                    win.blit(self.font.accent_color_menu_buttons[i+(self.num_of_buttons*j)],
                             (self.x + (self.button_width/2 - self.font.accent_color_menu_buttons[i+(self.num_of_buttons*j)].get_width()/2),
                              self.y + (self.button_height/2 - self.font.accent_color_menu_buttons[i+(self.num_of_buttons*j)].get_height()/2)))
                else:
                    pygame.draw.rect(win,
                                     self.color.set_color("button"),
                                     (self.x, self.y,
                                      self.button_width, self.button_height))
                    win.blit(self.font.accent_color_menu_buttons[i+(self.num_of_buttons*j)],
                             (self.x + (self.button_width/2 - self.font.accent_color_menu_buttons[i+(self.num_of_buttons*j)].get_width()/2),
                              self.y + (self.button_height/2 - self.font.accent_color_menu_buttons[i+(self.num_of_buttons*j)].get_height()/2)))

    def go_back_button(self, mouse_pos, win):
        """
        Defining where to draw go back to menu button
        """
        if mouse_pos[0] > 360 and mouse_pos[0] < 440 and mouse_pos[1] > 560 and mouse_pos[1] < 590:

            pygame.draw.rect(win, self.color.set_color("accent"), (self.width/2 - 40, 560, 80, 30))
            win.blit(self.font.draw_text("go_back_button_text"),
                     (self.width/2 - 40 + (80/2 - self.font.draw_text("go_back_button_text").get_width()/2),
                      560 + (30/2 - self.font.draw_text("go_back_button_text").get_height()/2)))
        else:
            pygame.draw.rect(win, self.color.set_color("button"), (self.width/2 - 40, 560, 80, 30))
            win.blit(self.font.draw_text("go_back_button_text"),
                     (self.width/2 - 40 + (80/2 - self.font.draw_text("go_back_button_text").get_width()/2),
                      560 + (30/2 - self.font.draw_text("go_back_button_text").get_height()/2)))

    def title(self, win, title):
        """
        Defining where to draw the title
        """
        win.blit(self.font.draw_text(title),
                 (self.width/2 - self.font.draw_text(title).get_width()/2, 20))

    def draw_game_buttons(self, mouse_pos, win):
        """
        Draw on screen
        """
        win.fill(self.color.set_color("background"))
        self.game_button(mouse_pos, win)
        self.go_back_button(mouse_pos, win)

    def draw_main_menu(self, mouse_pos, win):
        """
        Draw on screen
        """
        win.fill(self.color.set_color("background"))
        self.title(win, "main_menu_title_text")
        self.main_menu_button(mouse_pos, win)

    def draw_settings_menu(self, mouse_pos, win):
        """
        Draw on screen
        """
        win.fill(self.color.set_color("background"))
        self.title(win, "settings_menu_title_text")
        self.settings_menu_button(mouse_pos, win)
        self.go_back_button(mouse_pos, win)

    def draw_theme_menu(self, mouse_pos, win):
        """
        Draw on screen
        """
        win.fill(self.color.set_color("background"))
        self.title(win, "theme_menu_title_text")
        self.theme_menu_button(mouse_pos, win)
        self.go_back_button(mouse_pos, win)

    def draw_accent_color_menu(self, mouse_pos, win):
        """
        Draw on screen
        """
        win.fill(self.color.set_color("background"))
        self.title(win, "accent_color_menu_title_text")
        self.accent_color_menu_button(mouse_pos, win)
        self.go_back_button(mouse_pos, win)
