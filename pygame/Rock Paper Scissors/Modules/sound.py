import pygame

pygame.mixer.init()

class Sound:
    """
    Defining game and button sounds
    """
    def __init__(self):
        self.button_hover_sound = pygame.mixer.Sound('Assets/button_hover.wav')
        self.button_click_sound = pygame.mixer.Sound('Assets/button_click.wav')
        self.winner_sound = pygame.mixer.Sound('Assets/win.wav')
        self.looser_sound = pygame.mixer.Sound('Assets/loose.wav')
        self.tie_game_sound = pygame.mixer.Sound('Assets/tie.wav')
        self.muted = False

    def button_sound(self, sound):
        """
        Check if sound is muted and play the sound for button hover and click if not muted
        """
        if not self.muted:
            if sound == self.button_hover_sound:
                self.button_hover_sound.play()
            elif sound == self.button_click_sound:
                self.button_click_sound.play()

    def game_sound(self, sound):
        """
        Check if sound muted and play the sound for game when player wins,
        looses, or game is tie, if not muted
        """
        if not self.muted:
            if sound == self.winner_sound:
                self.winner_sound.play()
            elif sound == self.looser_sound:
                self.looser_sound.play()
            elif sound == self.tie_game_sound:
                self.tie_game_sound.play()

    def toggle_mute(self):
        """
        Toggle mute
        """
        self.muted = not self.muted
