class Color:
    """
    Defining colors and theme
    """
    def __init__(self):
        self.gray = (40, 40, 40)
        self.gray_2 = (140, 140, 140)
        self.light_gray = (200, 200, 200)
        self.dark_gray = (25, 25, 25)
        self.teal = (35, 104, 91)
        self.light_teal = (47, 145, 127)
        self.red = (255, 0, 0)
        self.blue = (0, 109, 204)
        self.green = (24, 204, 0)
        self.voilet = (115, 40, 181)
        self.orange = (204, 102, 0)
        self.pink = (217, 65, 171)
        self.yellow = (204, 187, 0)
        self.magenta = (199, 30, 78)
        self.lime = (145, 199, 30)
        self.purple = (168, 30, 199)

        # Defining themes
        self.theme_colors = {
            "light": {
                "text": self.dark_gray,
                "button": self.gray_2,
                "background": self.light_gray,
                "accent": self.light_teal
            },
            "dark": {
                "text": self.light_gray,
                "button": self.gray,
                "background": self.dark_gray,
                "accent": self.teal
            }
        }

        self.default_theme = "dark"

        self.available_themes = list(self.theme_colors.keys())

    def set_color(self, item):
        """
        Takes key as an argument and sets color of the given key,
        with the selected theme colors.
        """
        return self.theme_colors[self.default_theme].get(item)

    def set_accent_color(self, dark_color, light_color):
        """
        Change accent color
        """
        if self.default_theme == "dark":
            print(self.theme_colors["dark"]["accent"])
            self.theme_colors["dark"]["accent"] = dark_color
            print(self.theme_colors["dark"]["accent"])
        elif self.default_theme == "light":
            print(self.theme_colors["light"]["accent"])
            self.theme_colors["light"]["accent"] = light_color
            print(self.theme_colors["light"]["accent"])

    def set_theme(self, theme):
        """
        Set theme to dark or light
        """
        self.default_theme = theme
        print(self.default_theme)
