import pygame
from Modules.game import Game
from Modules.button import Button
from Modules.sound import Sound
from Modules.color import Color
from Modules.font import Font

pygame.display.set_caption('Rock Paper Scissor')

class Event:
    """
    Defining main game event loop
    """
    def __init__(self):
        self.width, self.height = 800, 600
        self.win = pygame.display.set_mode((self.width, self.height))
        self.fps = 60
        self.clock = pygame.time.Clock()
        self.run = True
        self.sound = Sound()
        self.color = Color()
        self.font = Font(self.color)
        self.button = Button(self.sound, self.color, self.font, self.width, self.height)
        self.game = Game(self.sound, self.color, self.font, self.width, self.height)

    def main_game(self):
        """
        Game event
        """
        while self.run:
            mouse_pos = pygame.mouse.get_pos()
            self.clock.tick(self.fps)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.run = False

                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        mouse_pos = pygame.mouse.get_pos()
                        if mouse_pos[0] > 360 and mouse_pos[0] < 440 and mouse_pos[1] > 560 and mouse_pos[1] < 590:
                            print("Menu")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.main_menu()
                        else:
                            print(mouse_pos)
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.game.logic(self.win)
                            break

            self.button.draw_game_buttons(mouse_pos, self.win)
            self.game.draw(self.win)
            pygame.display.update()

        pygame.quit()

    def main_menu(self):
        """
        Main menu event
        """
        while self.run:
            mouse_pos = pygame.mouse.get_pos()
            self.clock.tick(self.fps)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.run = False

                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        mouse_pos = pygame.mouse.get_pos()
                        print(mouse_pos)
                        if mouse_pos[0] > 280 and mouse_pos[0] < 520 and mouse_pos[1] < 220 and mouse_pos[1] > 170:
                            print("Play")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.main_game()
                        elif mouse_pos[0] > 280 and mouse_pos[0] < 520 and mouse_pos[1] < 290 and mouse_pos[1] > 240:
                            print("Settings")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.settings_menu()
                        elif mouse_pos[0] > 280 and mouse_pos[0] < 520 and mouse_pos[1] < 360 and mouse_pos[1] > 310:
                            print("About")
                        elif mouse_pos[0] > 280 and mouse_pos[0] < 520 and mouse_pos[1] < 430 and mouse_pos[1] > 380:
                            print("Quit")
                            pygame.quit()
                        else:
                            print("OOPS! Error!")

            self.button.draw_main_menu(mouse_pos, self.win)
            pygame.display.update()

        pygame.quit()

    def settings_menu(self):
        """
        Settings menu event
        """
        while self.run:
            mouse_pos = pygame.mouse.get_pos()
            self.clock.tick(self.fps)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.run = False

                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        mouse_pos = pygame.mouse.get_pos()
                        print(mouse_pos)
                        if mouse_pos[0] > 280 and mouse_pos[0] < 520 and mouse_pos[1] < 270 and mouse_pos[1] > 220:
                            print("Toggle sound")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.sound.toggle_mute()
                            print(self.sound.muted)
                        elif mouse_pos[0] > 280 and mouse_pos[0] < 520 and mouse_pos[1] < 340 and mouse_pos[1] > 290:
                            print("Accent")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.accent_color_menu()
                        elif mouse_pos[0] > 280 and mouse_pos[0] < 520 and mouse_pos[1] < 410 and mouse_pos[1] > 360:
                            print("Theme")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.theme_menu()
                        elif mouse_pos[0] > 360 and mouse_pos[0] < 440 and mouse_pos[1] > 560 and mouse_pos[1] < 590:
                            print("Menu")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.main_menu()
                        else:
                            print("OOPS! Error!")

            self.button.draw_settings_menu(mouse_pos, self.win)
            pygame.display.update()

        pygame.quit()

    def theme_menu(self):
        """
        Theme menu event
        """
        while self.run:
            mouse_pos = pygame.mouse.get_pos()
            self.clock.tick(self.fps)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.run = False

                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        mouse_pos = pygame.mouse.get_pos()
                        print(mouse_pos)
                        if mouse_pos[0] > 280 and mouse_pos[0] < 520 and mouse_pos[1] < 300 and mouse_pos[1] > 250:
                            print("Light theme")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.color.set_theme("light")
                        elif mouse_pos[0] > 280 and mouse_pos[0] < 520 and mouse_pos[1] < 370 and mouse_pos[1] > 320:
                            print("Dark theme")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.color.set_theme("dark")
                        elif mouse_pos[0] > 360 and mouse_pos[0] < 440 and mouse_pos[1] > 560 and mouse_pos[1] < 590:
                            print("Menu")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.settings_menu()
                        else:
                            print("OOPS! Error!")

            self.font.update_draw_text()
            self.button.draw_theme_menu(mouse_pos, self.win)
            pygame.display.update()

        pygame.quit()

    def accent_color_menu(self):
        """
        Accent color menu event
        """
        while self.run:
            mouse_pos = pygame.mouse.get_pos()
            self.clock.tick(self.fps)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.run = False

                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        mouse_pos = pygame.mouse.get_pos()
                        print(mouse_pos)
                        if mouse_pos[0] > 160 and mouse_pos[0] < 400 and mouse_pos[1] > 120 and mouse_pos[1] < 170:
                            print("Blue")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.color.set_accent_color(self.color.blue, self.color.blue)
                        elif mouse_pos[0] > 420 and mouse_pos[0] < 660 and mouse_pos[1] > 120 and mouse_pos[1] < 170:
                            print("Gray")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.color.set_accent_color(self.color.light_gray, self.color.light_gray)
                        elif mouse_pos[0] > 160 and mouse_pos[0] < 400 and mouse_pos[1] > 190 and mouse_pos[1] < 240:
                            print("Green")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.color.set_accent_color(self.color.green, self.color.green)
                        elif mouse_pos[0] > 420 and mouse_pos[0] < 660 and mouse_pos[1] > 190 and mouse_pos[1] < 240:
                            print("Yellow")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.color.set_accent_color(self.color.yellow, self.color.yellow)
                        elif mouse_pos[0] > 160 and mouse_pos[0] < 400 and mouse_pos[1] > 260 and mouse_pos[1] < 310:
                            print("Red")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.color.set_accent_color(self.color.red, self.color.red)
                        elif mouse_pos[0] > 420 and mouse_pos[0] < 660 and mouse_pos[1] > 260 and mouse_pos[1] < 310:
                            print("Magenta")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.color.set_accent_color(self.color.magenta, self.color.magenta)
                        elif mouse_pos[0] > 160 and mouse_pos[0] < 400 and mouse_pos[1] > 330 and mouse_pos[1] < 380:
                            print("Purple")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.color.set_accent_color(self.color.purple, self.color.purple)
                        elif mouse_pos[0] > 420 and mouse_pos[0] < 660 and mouse_pos[1] > 330 and mouse_pos[1] < 380:
                            print("Lime")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.color.set_accent_color(self.color.lime, self.color.lime)
                        elif mouse_pos[0] > 160 and mouse_pos[0] < 400 and mouse_pos[1] > 400 and mouse_pos[1] < 450:
                            print("Orange")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.color.set_accent_color(self.color.orange, self.color.orange)
                        elif mouse_pos[0] > 420 and mouse_pos[0] < 660 and mouse_pos[1] > 400 and mouse_pos[1] < 450:
                            print("Voilet")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.color.set_accent_color(self.color.voilet, self.color.voilet)
                        elif mouse_pos[0] > 160 and mouse_pos[0] < 400 and mouse_pos[1] > 470 and mouse_pos[1] < 520:
                            print("Pink")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.color.set_accent_color(self.color.pink, self.color.pink)
                        elif mouse_pos[0] > 420 and mouse_pos[0] < 660 and mouse_pos[1] > 470 and mouse_pos[1] < 520:
                            print("Teal")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.color.set_accent_color(self.color.teal, self.color.teal)
                        elif mouse_pos[0] > 360 and mouse_pos[0] < 440 and mouse_pos[1] > 560 and mouse_pos[1] < 590:
                            print("Menu")
                            self.sound.button_sound(self.sound.button_click_sound)
                            pygame.time.delay(1000)
                            self.settings_menu()
                        else:
                            print("OOPS! Error!")

            self.button.draw_accent_color_menu(mouse_pos, self.win)
            pygame.display.update()

        pygame.quit()

Event().main_menu()
