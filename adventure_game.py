import time
import os
import sys
import random

# Items Class, contains dictionaries of weapons, potions and armors.
class Items:
    def __init__(self):
        self.items = {
            "weapons": {                          # defining weapons
                "rusty_sword": {
                    "name": "Rusty Sword",
                    "damage": 5,
                    "rarity": "Common",
                    "quality": 25
                },
                "better_sword": {
                    "name": "Better Sword",
                    "damage": 10,
                    "rarity": "Epic",
                    "quality": 50
                },
                "great_sword": {
                    "name": "Great Sword",
                    "damage": 20,
                    "rarity": "Legendary",
                    "quality": 100
                }
            },
            "potions": {                          # defining potions
                "basic_heal": {
                    "name": "Basic Healing",
                    "heals": 5
                },
                "better_heal": {
                    "name": "Better Healing",
                    "heals": 10
                },
                "great_heal": {
                    "name": "Great Healing",
                    "heals": 15
                }
            },
            "armor": {                            # defining armor
                "basic_armor": {
                    "name": "Basic Armor",
                    "defence": 20,
                    "rarity": "Common"
                },
                "better_armor": {
                    "name": "Better Armor",
                    "defence": 50,
                    "rarity": "Epic"
                },
                "great_armor": {
                    "name": "Great Armor",
                    "defence": 100,
                    "rarity": "Legendary"
                }
            }
        }

# Weapons Class
class Weapons:
    def __init__(self):
        self.items = Items()

        self.weapon = None
        self.name = None
        self.damage = None
        self.rarity = None
        self.quality = None

    def weapons(self, name):
        self.weapon = self.items.items["weapons"][name]
        self.name = self.weapon["name"]
        self.damage = self.weapon["damage"]
        self.rarity = self.weapon["rarity"]
        self.quality = self.weapon["quality"]
        return self.weapon

# Potions Class
class Potions:
    def __init__(self):
        self.items = Items()

        self.potion = None
        self.name = None
        self.heal = None

    def potions(self, name):
        self.potion = self.items.items["potions"][name]
        self.name = self.potion["name"]
        self.heal = self.potion["heals"]
        return self.potion

#Armor Class
class Armor:
    def __init__(self):
        self.items = Items()
 
        self.armor = None
        self.name = None
        self.defence = None
 
    def armors(self, name):
        self.armor = self.items.items["armor"][name]
        self.name = self.armor["name"]
        self.defence = self.armor["defence"]
        return self.armor

# Player Class
class Player:
    def __init__(self):
        self.items = Items()

        self.name = None
        self.health = None

    def player_name(self):
        self.name = input("\nEnter your character's name: ").lower()

    def player_health(self):
        self.health = 100
 
    def equipped_weapon(self, name):
        self.weapon = self.items.items["weapons"][name]
 
    def equipped_potions(self, name):
        self.potion = self.items.items["potions"][name]

    def equipped_armor(self, name):
        self.armor = self.items.items["armor"][name]

    def player_stats(self):
        print(f"\nYour Stats\n > Health: {player.health}\n > Weapon Quality: {weapon.quality}\n > Armor: {armor.defence}")

# Enemy Class
class Enemy:
    def __init__(self):
        self.enemy = {
            "enemy": {
                "small_minion": {                 # defining small minion
                    "name": "Small Minion",
                    "health": 20,
                    "damage": 5,
                    "aname": None,
                    "armor": 0
                },
                "big_minion": {                   # defining big minion
                    "name": "Big Minion",
                    "health": 40,
                    "damage": 10,
                    "aname": "Better Armor",
                    "armor": 10
                },
                "giant_minion": {                 # defining giant minion
                    "name": "Giant Minion",
                    "health": 80,
                    "damage": 20,
                    "aname": "Great Armor",
                    "armor": 50
                }
            }
        }

        self.spawn = None
        self.name = None
        self.health = None
        self.damage = None
        self.armor_name = None
        self.armor = None
 
    def enemy_spawn(self, name):
        self.spawn = self.enemy["enemy"][name]
        self.name = self.spawn["name"]
        self.health = self.spawn["health"]
        self.damage = self.spawn["damage"]
        self.armor_name = self.spawn["aname"]
        self.armor = self.spawn["armor"]
        return self.spawn
 
    def enemy_stats(self):
        print(f"\nEnemy Stats\n > Health: {enemy.health}\n > Armor: {enemy.armor}")



# Fight Loop 1 Enemy
def fight_loop_one_enemy():
    while True:
        fight_flee = input("\nDo you fight or flee? (fight/flee): ").lower()
        time.sleep(2)
        if fight_flee == "fight":
            while True:
                time.sleep(2)
                os.system("clear")

                player.player_stats()
                enemy.enemy_stats()

                if player.health <= 0:
                    print("\nYou're dead!")
                    time.sleep(2)
                    print("\nGAME OVER!")
                    break
                if enemy.health <=0:
                    print("\nEnemy is dead!")
                    break
                game_turn = "pl_turn"
                if game_turn == "pl_turn":
                    time.sleep(3)
                    os.system("clear")
                    print("\nYOUR TURN!\n")
                    time.sleep(2)
                    attack_block = input("Do you attack or block? (attack/blocks): ")
                    if attack_block == "attack":
                        if enemy.armor_name != None:
                            print(f"\nYou attack and {enemy.name}'s armor takes {weapon.damage} damage.")
                            enemy.armor -= weapon.damage
                            weapon.quality -= weapon.damage
                            if enemy.armor <= 0:
                                print(f"\nYou broke {enemy.name} armor, and will damage the enemy now.")
                                enemy.armor_name = None
                                time.sleep(2)
                        elif enemy.armor_name == None:
                            print(f"\nYou did {weapon.damage} damage")
                            enemy.health -= weapon.damage
                            weapon.quality -= weapon.damage
                        else:
                            print("\nError")
                    if attack_block == "block":
                        if player.equipped_armor == None:
                            print(f"\nYou don't have any armor equipped so you took {enemy.damage} damage.")
                            player.health -= enemy.damage
                        else:
                            print(f"\nYou blocked and your {armor.name} took damage.\n")
                            player.equipped_armor -= enemy.damage
                if enemy.health <= 0:
                    break
                game_turn = "en_turn"
                en_turn_choice = random.choice(["attack", "block"])
                if game_turn == "en_turn":
                    print("\nENEMY'S TURN!\n")
                    time.sleep(2)
                    if en_turn_choice == "attack":
                        if player.equipped_armor != None:
                            print(f"{enemy.name} attacks and your {armor.name} takes {enemy.damage} damage.")
                            armor.defence -= enemy.damage
                            if armor.defence <= 0:
                                print("\nYour armor broke, and will no longer protect you from enemy's damage.")
                                player.equipped_armor = None
                                time.sleep(2)
                        elif player.equipped_armor == None:
                            print(f"{enemy.name} attacks and you take {enemy.damage} damage.")
                            player.health -= enemy.damage
                        else:
                            print("\nError")
                    elif en_turn_choice == "block":
                            print(f"{enemy.name} blocks and takes {weapon.damage} damage.")
                            enemy.health -= weapon.damage
                            weapon.quality -= weapon.damage
                else:
                    print("No more turns left.")
            print(f"\nYou killed the {enemy.name}")
            time.sleep(2)
            break
        elif fight_flee == "flee":
            print("Well this part of the story is not yet written..")
            time.sleep(2)
            print("So...")
            time.sleep(2)
            print("GAME OVER!")
        else:
            print("Please select a valid option.")
            break

# Fight Loop
def fight_loop_two_enemy():
    while True:
        fight_flee = input("\nDo you fight or flee? (fight/flee): ").lower()
        time.sleep(2)
        if fight_flee == "fight":
            while True:
                time.sleep(2)
                os.system("clear")

                player.player_stats()
                enemy.enemy_stats()

                if player.health <= 0:
                    print("\nYou're dead!")
                    time.sleep(2)
                    print("\nGAME OVER!")
                    break
                if enemy.health <=0:
                    print("\nEnemy is dead!")
                    break
                game_turn = "pl_turn"
                if game_turn == "pl_turn":
                    time.sleep(3)
                    os.system("clear")
                    print("\nYOUR TURN!\n")
                    time.sleep(2)
                    attack_block = input("Do you attack or block? (attack/blocks): ")
                    if attack_block == "attack":
                        if enemy.armor_name != None:
                            print(f"\nYou attack and {enemy.name}'s armor takes {weapon.damage} damage.")
                            enemy.armor -= weapon.damage
                            weapon.quality -= weapon.damage
                            if enemy.armor <= 0:
                                print(f"\nYou broke {enemy.name} armor, and will damage the enemy now.")
                                enemy.armor_name = None
                                time.sleep(2)
                        elif enemy.armor_name == None:
                            print(f"You did {weapon.damage} damage")
                            enemy.health -= weapon.damage
                            weapon.quality -= weapon.damage
                        else:
                            print("\nError")
                    if attack_block == "block":
                        if player.equipped_armor == None:
                            print("\nYou don't have any armor equipped so you took damage.\n")
                            player.health -= enemy.damage
                        else:
                            print(f"\nYou blocked and your {armor.name} took damage.\n")
                            armor.defence -= enemy.damage
                if enemy.health <= 0:
                    break
                game_turn = "en1_turn"
                en1_turn_choice = random.choice(["attack", "block"])
                if game_turn == "en1_turn":
                    print("\nFIRST ENEMY'S TURN!\n")
                    time.sleep(2)
                    if en1_turn_choice == "attack":
                        if player.equipped_armor != None:
                            print(f"{enemy.name} attacks and your {armor.name} takes {enemy.damage} damage.")
                            armor.defence -= enemy.damage
                            if armor.defence <= 0:
                                print("Your armor broke, and will no longer protect you from enemy's damage.")
                                player.equipped_armor = None
                                time.sleep(2)
                        elif player.equipped_armor == None:
                            print(f"{enemy.name} attacks and you take {enemy.damage} damage.")
                            player.health -= enemy.damage
                        else:
                            print("\nError")
                    elif en1_turn_choice == "block":
                        print(f"{enemy.name} blocks and takes {weapon.damage} damage.")
                        enemy.health -= weapon.damage
                        weapon.quality -= weapon.damage
                if enemy.health <= 0:
                    break
                game_turn = "en2_turn"
                en2_turn_choice = random.choice(["attack", "block"])
                if game_turn == "en2_turn":
                    print("\nSECOND ENEMY'S TURN!\n")
                    time.sleep(2)
                    if en2_turn_choice == "attack":
                        if player.equipped_armor != None:
                            print(f"{enemy.name} attacks and your {armor.name} takes {enemy.damage} damage.")
                            armor.defence -= enemy.damage
                            if armor.defence <= 0:
                                print("Your armor broke, and will no longer protect you from enemy's damage.")
                                player.equipped_armor = None
                                time.sleep(2)
                        elif player.equipped_armor == None:
                            print(f"{enemy.name} attacks and you take {enemy.damage} damage.")
                            player.health -= enemy.damage
                        else:
                            print("\nError")
                    elif en2_turn_choice == "block":
                        print(f"{enemy.name} blocks and takes {weapon.damage} damage.")
                        enemy.health -= weapon.damage
                        weapon.quality -= weapon.damage
                else:
                    print("No more turns left.")
            print(f"\nYou killed the {enemy.name}")
            time.sleep(2)
            break
        elif fight_flee == "flee":
            print("Well this part of the story is not yet written..")
            time.sleep(2)
            print("So...")
            time.sleep(2)
            print("GAME OVER!")
        else:
            print("Please select a valid option.")
            break

# Check Inventory
def check_inventory():
    while True:
        check_inventory = input("\nDo you want to check your inventory or continue game? (check/continue): ")
        if check_inventory == "check":
            print(f"\nYour stats \n > Name: {player.name.upper()}\n > Health: {player.health}\n > Equipped weapon: {player.equipped_weapon}\n > Equipped armor: {player.equipped_armor}\n > Equipped potion: {player.equipped_potions}")
            break
        elif check_inventory == "continue":
            print("\nYou continue to move forward..")
            break
        else:
            print("Please select a valid option.")

# Use Potion
def use_potion():
    while True:
        use_potion = input(f"\nDo you want to use the {potion.name} potion? (yes/no): ")
        if use_potion == "yes":
            if player.equipped_potions != None:
                if player.health >= 100:
                    print(f"\nYou are already at max health {(player.health)}..")
                    time.sleep(2)
                    print("The potion will be saved to your inventory.")
                    break
                player.health += potion.heal
                player.equipped_potions = None
                break
            elif player.equipped_potions == None:
                print("You do not have any potion in your inventory.")
                break
        elif use_potion == "no":
            print(f"\nYou didn't use the {potion.name} potion, and is stored in your inventory.")
            break
        else:
            print("Please select a valid option.")

# Instantiate Methods
items = Items()
enemy = Enemy()
player = Player()
weapon = Weapons()
potion = Potions()
armor = Armor()

# Game Begins
print("##############################")
print("#                            #")
print("# Welcome to adventure time! #")
print("#                            #")
print("#          > Play            #")
print("#          > Exit            #")
print("#                            #")
print("##############################")

time.sleep(2)

player.player_name()
player.player_health()

#Check if player wants to play.
while True:
    wants_to_play = input("\nDo you want play? (yes/no): ").lower()
    if wants_to_play == "yes":
        os.system("clear")
        print("\nWelcome " + player.name.upper() + " to adventure time!\n")
        time.sleep(2)
        print("The game begins!")
        time.sleep(2)
        print("\nYou start with " + str(player.health) + " health.\n")
        time.sleep(2)
        os.system("clear")
        break
    elif wants_to_play == "no":
        print("Hope we see you again!")
        exit()
    else:
        print("\nPlease select a valid option.")

print("You wake up near a shore,")
time.sleep(2)
print("there is no one around you,")
time.sleep(2)
print("the storm coming your way,")
time.sleep(2)
print("you see a boat and a hut nearby,")
time.sleep(2)

while True:
    hut_boat = input("where would you go? (hut/boat): ").lower()
    if hut_boat == "hut":
        print("\nYou survived the night, you got " + str(player.health) + " health remaining.\n")
        time.sleep(2)
        break
    elif hut_boat == "boat":
        print("You didn't survive the storm, and died!")
        time.sleep(2)
        print("GAME OVER!")
        exit()
    else:
        print("\nPlease select a valid option.\n")

print("You wake up in the morning,")
time.sleep(2)
print("the boat is wrecked,")
time.sleep(2)
print("you can't go back,")
time.sleep(2)
print("you decide to move forward,")
time.sleep(2)
print("you see two paths, one on your left and one on your right")
time.sleep(2)

while True:
    left_right = input("where do you wanna go? (left/right): ").lower()
    if left_right == "left":
        print("\nYou found a weapon,")
        time.sleep(2)
        while True:
            pick_leave = input("do you pick it up or leave? (pick/leave): ").lower()
            if pick_leave == "pick":
                player.equipped_weapon = weapon.weapons("rusty_sword")
                player.equipped_potions = None
                player.equipped_armor = None
                print(f"\nYou found a {weapon.name}\n > Damage: {weapon.damage}\n > Quality: {weapon.quality}\n > Rarity: {weapon.rarity}")
                time.sleep(2)
                print("\nYou continue to move forward..\n")
                time.sleep(2)
                os.system("clear")
                break
            elif pick_leave == "leave":
                print("\nYou didn't pick up the weapon, and continue to move forward..\n")
                break
            else:
                print("\nPlease select a valid option.\n")
        break
    elif left_right == "right":
        print("You fell into a hole, and died!")
        time.sleep(2)
        print("GAME OVER!")
        exit()
    else:
        print("\nPlease slect a valid option.\n")

print("You see a house ahead,")
time.sleep(2)
print("you enter the house,")
time.sleep(2)
print("you see a door on left and right,")
time.sleep(2)

while True:
    left_right = input("which door you go to? (left/right): ").lower()
    if left_right == "left":
        enemy.spawn = enemy.enemy_spawn("small_minion")
        print(f"\n{enemy.name} appears...")
        time.sleep(2)
        print(f" > Health: {enemy.health} \n > Damage: {enemy.damage} \n > Armor: {enemy.armor}")
        print("\nIt starts to attack you,")
        time.sleep(2)

        fight_loop_one_enemy()
        break

time.sleep(2)
print(f"\n\n-- You dropped the {weapon.name} --\n")
time.sleep(2)

player.equipped_weapon = weapon.weapons("better_sword")
player.equipped_potions = potion.potions("basic_heal")
player.equipped_armor = None

print(f"\nYou found a {weapon.name}\n > Damage: {weapon.damage}\n > Quality: {weapon.quality}\n > Rarity: {weapon.rarity}")
time.sleep(2)
print(f"\nYou found a {potion.name}\n > Heals: {potion.heal}")
time.sleep(2)

use_potion()

check_inventory()

time.sleep(3)
os.system("clear")
time.sleep(2)
print("You exit the house,")
time.sleep(2)
print("\nA unknown man approaches you,")
time.sleep(2)
print("he takes you to a mill nearby,")
time.sleep(2)
print("upon reaching the mill, you see a shiny crate,")
time.sleep(2)

while True:
    open_crate = input("do you reach and open the crate or keep following the unknown man? (open/follow): ")
    if open_crate == "open":

        player.equipped_weapon = weapon.weapons("better_sword")
        player.equipped_potions = armor.armors("basic_armor")
        player.equipped_armor = potion.potions("better_heal")

        print(f"\nYou found a {armor.name}\n > Defence: {armor.defence}")
        time.sleep(2)
        print(f"\nYou found a {potion.name}\n > Heals: {potion.heal}")
        time.sleep(2)

        use_potion()

        check_inventory()

        print("\nYou continue to follow the unknown man..\n")
        time.sleep(2)
        break
    elif open_crate == "follow":
        print("\nYou didn't open the crate and continue to follow the unknown man..")
    else:
        print("Please select a valid option.")


time.sleep(3)
os.system("clear")
print("The unknown man tells you,")
time.sleep(2)
print("that his daughter has been kidnapped by some minions,")
time.sleep(2)
print("unknown man says that he lives nearby,")
time.sleep(2)
print("and the mill is his property,")
time.sleep(2)
print("and his daughter works in the mill with him.")
time.sleep(2)
print("\nHe tells you that his daughter went out to get some supplies,")
time.sleep(2)
print("and never returned back home.")
time.sleep(2)
print("\nHe tells you that if you help him find his daughter,")
time.sleep(2)
print("he will reward you, and help you get out of the island.")
time.sleep(2)

while True:
    help_unknown_man = input("Do you help the unknown man or deny? (help/deny): ")
    if help_unknown_man == "help":
        print("\nYou chose to help.")
        time.sleep(2)
        os.system("clear")
        print("\nYou enter the nearby village,")
        time.sleep(2)
        print("you start questioning people about the unknown man's daughter,")
        time.sleep(2)
        print("someone tells you to look near the abandoned house near the river,")
        time.sleep(2)
        print("you travel to the abandoned house near the river..")
        time.sleep(2)

        enemy.spawn = enemy.enemy_spawn("big_minion")

        print(f"\n{enemy.name} appears...")
        time.sleep(2)
        print(f" > Health: {enemy.health} \n > Damage: {enemy.damage} \n > Armor: {enemy.armor}")
        print("\nIt starts to attack you,")
        time.sleep(2)

        fight_loop_one_enemy()
        break

    elif help_unknown_man == "deny":
        print("Well this part of the story is not yet written..")
        time.sleep(2)
        print("So...")
        time.sleep(2)
        print("GAME OVER!")
        exit()
    else:
        print("Please select a valid option.")

time.sleep(2)
print(f"\n\n-- You dropped the {weapon.name} --\n")
time.sleep(2)

player.equipped_weapon = weapon.weapons("better_sword")
player.equipped_potions = armor.armors("better_armor")
player.equipped_armor = potion.potions("better_heal")

print(f"\nYou found a new {weapon.name}\n > Damage: {weapon.damage}\n > Quality: {weapon.quality}\n > Rarity: {weapon.rarity}")
time.sleep(2)
print(f"\nThe old {weapon.name} will be replaced by this.")
time.sleep(2)
print(f"\nYou found a {armor.name}\n > Defence: {armor.defence}")
time.sleep(2)
print(f"\nYou found a {potion.name}\n > Heals: {potion.heal}")
time.sleep(2)

use_potion()

check_inventory()

time.sleep(3)
os.system("clear")
print("\nYou found a note,")
time.sleep(2)
print("it says the location of the unknown man's daughter,")
time.sleep(2)
print("the basement of unknown man's mill.")
time.sleep(2)
print("\nYou travel to back to the mill,")
time.sleep(2)
print("you tell the unknown man that his daughter is in the mill's basement,")
time.sleep(2)

enemy.spawn = enemy.enemy_spawn("small_minion")

print(f"\nTwo {enemy.name} appears...")
time.sleep(2)
print(f" > Health: {enemy.health} \n > Damage: {enemy.damage} \n > Armor: {enemy.armor}")
print("\nIt starts to attack you,")
time.sleep(2)

fight_loop_two_enemy()

time.sleep(3)
os.system("clear")
print("You are wounded,")
time.sleep(2)
print("\nThe unknown man gives you some potions, weapons and armor\n")
time.sleep(2)
print(f"\n-- You dropped the {weapon.name} --\n")
time.sleep(2)
print(f"\n-- You dropped the {armor.name} --\n")
time.sleep(2)

player.equipped_weapon = weapon.weapons("great_sword")
player.equipped_potions = armor.armors("great_armor")
player.equipped_armor = potion.potions("great_heal")

print(f"\nYou picked up {weapon.name}\n > Damage: {weapon.damage}\n > Quality: {weapon.quality}\n > Rarity: {weapon.rarity}")
time.sleep(2)
print(f"\nYou picked up {armor.name}\n > Defence: {armor.defence}")
time.sleep(2)
print(f"\nYou picked up {potion.name}\n > Heals: {potion.heal}")
time.sleep(2)

use_potion()

check_inventory()

print("\nYou enter the basement.")
time.sleep(2)
print("its dark here..")
time.sleep(2)

enemy.spawn = enemy.enemy_spawn("giant_minion")

print(f"\nTwo {enemy.name} appears...")
time.sleep(2)
print(f" > Health: {enemy.health} \n > Damage: {enemy.damage} \n > Armor: {enemy.armor}")
print("\nIt starts to attack you,")
time.sleep(2)

fight_loop_two_enemy()

print("You won the fight,")
time.sleep(2)
print("you found unknown man's daughter..")
time.sleep(2)
print("\nThe unknown man thanks you for finding his daughter.")
time.sleep(2)
print("\nCONGRATULATIONS! You have successfully completed the game.")
time.sleep(2)
print("You are now free to roam the streets of adventure city..")
time.sleep(2)
print("\n---- GOOD BYE! ----")
time.sleep(2)
print("\nThank you for playing,")
time.sleep(2)
print("Hope we see you again.")

exit()
