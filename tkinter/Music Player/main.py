from tkinter import *
from tkinter import filedialog
import tkinter.ttk as ttk
import time
import mutagen
from mutagen.oggvorbis import OggVorbis
import pygame

pygame.mixer.init()

class Event():
    def __init__(self):
        self.root = Tk()
        self.title = self.root.title('Music player')
        self.win_size = self.root.geometry("320x200")
        self.paused = False
        self.stopped = False
        self.playlist_is_hidden = True
        self.library_is_hidden = True
        self.muted = False
        self.img1 = PhotoImage(file='play.png')
        self.img2 = PhotoImage(file='pause.png')
        self.img3 = PhotoImage(file='stop.png')
        self.img4 = PhotoImage(file='next.png')
        self.img5 = PhotoImage(file='prev.png')

    def playlist_frame(self):
        global playlist_frame_label
        playlist_frame_label = LabelFrame(self.root,
                                          text="Playlist",
                                          padx=10, pady=10)
#        playlist_frame_label.grid(row=4, column=0,
#                                  padx=10, pady=10,
#                                  columnspan=6)
        playlist_frame_label.pack(fill=BOTH, expand=1, padx=10, pady=10)

    def playlist_button(self):
        add_button = Button(playlist_frame_label,
                            text="+",
                            command=self.add_music)
        add_folder_button = Button(playlist_frame_label,
                                   text="*",
                                   command=self.add_music_folder)
        remove_button = Button(playlist_frame_label,
                                   text="-",
                                   command=self.remove_track)
        clear_button = Button(playlist_frame_label,
                                   text="C",
                                   command=self.clear_playlist)
        show_library_button = Button(playlist_frame_label,
                                     text="L",
                                     command=self.show_hide_library)
        pl_btn = [add_button, add_folder_button, remove_button, clear_button, show_library_button]

        count = 0
        for btn in pl_btn:
        #    count +=1
            Grid.rowconfigure(playlist_frame_label, 4, weight=1)
            Grid.columnconfigure(playlist_frame_label, count, weight=1)
            btn.grid(row=4, column=count, stick=N+S+E+W)
            count +=1
#        add_button.grid(row=6, column=0, stick=N+S+E+W)
#        add_folder_button.grid(row=6, column=1, stick=N+S+E+W)
#        remove_button.grid(row=6, column=2, stick=N+S+E+W)
#        clear_button.grid(row=6, column=3, stick=N+S+E+W)
#        show_library_button.grid(row=6, column=4, stick=N+S+E+W)
#        add_button.pack(fill=BOTH, expand=1, padx=5, pady=5)
#        add_folder_button.pack(fill=BOTH, expand=1, padx=5, pady=5)
#        remove_button.pack(fill=BOTH, expand=1, padx=5, pady=5)
#        clear_button.pack(fill=BOTH, expand=1, padx=5, pady=5)
#        show_library_button.pack(fill=BOTH, expand=1, padx=5, pady=5)

    def playlist_list_box(self):
        global playlist_box
        playlist_box = Listbox(playlist_frame_label, width=60,
                               bd=2, relief=SUNKEN,
                               bg="black", fg="lightgreen")

        Grid.rowconfigure(playlist_frame_label, 0, weight=1)
        Grid.columnconfigure(playlist_frame_label, 5, weight=1)

        playlist_box.grid(row=3, column=0,
                          padx=10, pady=(0, 10),
                          columnspan=5, sticky=N+S+E+W)
#        playlist_box.pack(fill=BOTH, expand=1, padx=10, pady=10)

    def playlist_tree_view(self):
        global lib_win
        lib_win = Toplevel()
        lib_win.title('Library')
        lib_win.geometry("600x400")

        global playlist_tree_frame
        playlist_tree_frame = LabelFrame(lib_win,
                                          text="Library",
                                          padx=10, pady=10)
#        playlist_tree_frame.grid(row=0, column=0,
#                                  padx=10, pady=10,
#                                 sticky=W+E+N+S)
        playlist_tree_frame.pack(fill=BOTH, expand=1, padx=10, pady=10)

        global playlist_tree
        playlist_tree = ttk.Treeview(playlist_tree_frame)

        playlist_tree['columns'] = ("#", "Title", "Artist", "Album", "Genre", "Date")

        playlist_tree.column('#0', width=0, stretch=NO)
        playlist_tree.column('#', anchor=CENTER, width=20)
        playlist_tree.column('Title', anchor=W, width=100)
        playlist_tree.column('Artist', anchor=W, width=100)
        playlist_tree.column('Album', anchor=W, width=100)
        playlist_tree.column('Genre', anchor=W, width=80)
        playlist_tree.column('Date', anchor=W, width=50)

        playlist_tree.heading('#0', text='', anchor=W)
        playlist_tree.heading('#', text='#', anchor=CENTER)
        playlist_tree.heading('Title', text='Title', anchor=W)
        playlist_tree.heading('Artist', text='Artist', anchor=W)
        playlist_tree.heading('Album', text='Album', anchor=W)
        playlist_tree.heading('Genre', text='Genre', anchor=W)
        playlist_tree.heading('Date', text='Date', anchor=W)

#        playlist_tree.grid(row=0, column=0, ipadx=60, ipady=50, sticky=W+E+N+S)
        playlist_tree.pack(fill=BOTH, expand=1, padx=5, pady=5)

    def player_frame(self):
        global player_frame_label
        player_frame_label = LabelFrame(self.root,
                                        text="Now playing",
                                        padx=10, pady=10)
#        player_frame_label.grid(row=0, column=0,
#                                padx=10, pady=10,
#                                columnspan=6)
        player_frame_label.pack(fill=BOTH, expand=1, padx=10, pady=10)

    def player_button(self):
        play_button = Button(player_frame_label, image=self.img1, borderwidth=0, command=self.play)
        pause_button = Button(player_frame_label, image=self.img2, borderwidth=0, command=self.pause)
        stop_button = Button(player_frame_label, image=self.img3, borderwidth=0, command=self.stop)
        next_button = Button(player_frame_label, image=self.img4, borderwidth=0, command=self.next_track)
        previous_button = Button(player_frame_label, image=self.img5, borderwidth=0, command=self.prev_track)
        show_playlist_button = Button(player_frame_label, text="Pl", command=self.show_hide_playlist)
#        previous_button.grid(row=2, column=0)
#        play_button.grid(row=2, column=1)
#        pause_button.grid(row=2, column=2)
#        stop_button.grid(row=2, column=3)
#        next_button.grid(row=2, column=4)
#        show_playlist_button.grid(row=2, column=5)

        pl_btn = [play_button, pause_button, stop_button, previous_button, next_button, show_playlist_button]

        count = 0
        for btn in pl_btn:
        #    count +=1
            Grid.rowconfigure(player_frame_label, 2, weight=1)
            Grid.columnconfigure(player_frame_label, count, weight=1)
            btn.grid(row=2, column=count, stick=N+S+E+W)
            count +=1

#    def volume_frame(self):
#        global vol_frame
#        vol_frame = LabelFrame(player_frame_label, text="Volume")
#        vol_frame.grid(row=4, column=3, columnspan=2)

    def volume_slider(self):
        global vol_slider
        vol_slider = ttk.Scale(player_frame_label, from_=0, to=1,
                               orient=HORIZONTAL, value=1,
                               command=self.volume, length=80)

        Grid.rowconfigure(player_frame_label, 1, weight=1)
        Grid.columnconfigure(player_frame_label, 0, weight=1)

        vol_slider.grid(row=0, column=3, columnspan=3, sticky=E+W)
    #    vol_slider.pack()

    def volume_mute_button(self):
        global mute_button
        mute_button = ttk.Button(player_frame_label, text="<))", command=self.volume_mute)
        mute_button.grid(row=0, column=2, padx=(50, 0))

    def status_bar(self):
        global song_length_label
        song_length_label = Label(player_frame_label, text='--:--/--:--',
                                  bd=8, relief=SUNKEN, anchor=E,
                                  bg="black", fg="green", font=('Monospace', 10))

        Grid.rowconfigure(player_frame_label, 0, weight=1)
        Grid.columnconfigure(player_frame_label, 0, weight=1)

        song_length_label.grid(row=0, column=0, pady=(0, 10),
                               columnspan=2, ipadx=10, ipady=10,
                               sticky=N+S+E+W)

    def seek_bar_scale(self):
        global seek_bar
        seek_bar = ttk.Scale(player_frame_label, from_=0, to=100,
                         orient=HORIZONTAL, command=self.seek_bar,
                         length=200)

        Grid.rowconfigure(player_frame_label, 1, weight=1)
        Grid.columnconfigure(player_frame_label, 0, weight=1)

        seek_bar.grid(row=1, column=0, pady=10, columnspan=6, sticky=E+W)

    def play_time(self):
        if self.stopped:
            return

        song_position = pygame.mixer.music.get_pos() // 1000

        current_time = time.strftime('%M:%S', time.gmtime(song_position))

        song = playlist_box.get(ACTIVE)
        song = f'/home/apoorv/Music/{song}.ogg'

        global song_length
        song_load = OggVorbis(song)
        song_length = song_load.info.length

        mg = mutagen.File(song, easy=True)
#        print(mg['title'], mg['artist'])
#        print(mg)

        song_length_info = time.strftime('%M:%S', time.gmtime(song_length))

        song_position += 1

        if int(seek_bar.get()) == int(song_length):
            song_length_label.config(text=f'{song_length_info}/{song_length_info}')
        elif self.paused or self.stopped:
            pass
        elif int(seek_bar.get()) == int(song_position):
            seek_bar.config(to=int(song_length), value=int(song_position))
        else:
            seek_bar.config(to=int(song_length), value=int(seek_bar.get()))
            current_time = time.strftime('%M:%S', time.gmtime(seek_bar.get()))
            song_length_label.config(text=f'{current_time}/{song_length_info}')

            new_time = int(seek_bar.get()) + 1
            seek_bar.config(value=new_time)

#        temp = Label(player_frame_label, text=f"Seek: {seek_bar.get()} | SongPos: {int(song_position)} | SLI: {song_length_info}")
#        temp.grid(row=0, column=2)
        print(f"Seek: {seek_bar.get()} | SongPos: {int(song_position)} | SLI: {song_length_info}")

#        seek.config(value=song_position)

        song_length_label.after(1000, self.play_time)

    def add_music(self):
        song = filedialog.askopenfilename(initialdir="~/Music",
                                          title="Select a file..",
                                          filetypes=(("ogg files", "*.ogg"),
                                                     ("all files", "*.*")))

        song = song.replace("/home/apoorv/Music/", "")
        song = song.replace(".ogg", "")

        playlist_box.insert(END, song)

    def add_music_folder(self):
        songs = filedialog.askopenfilenames(initialdir="~/Music",
                                            title="Select files..",
                                            filetypes=(("ogg files", "*.ogg"),
                                                       ("all files", "*.*")))

        for song in songs:
            song = song.replace("/home/apoorv/Music/", "")
            song = song.replace(".ogg", "")

            playlist_box.insert(END, song)

        count=0
        for song in songs:
            count += 1
            mg = mutagen.File(song, easy=True)

            playlist_tree.insert(parent='', index='end', iid=count, text='',
                                 values=(mg.get('tracknumber', ''), mg.get('title', ''),
                                         mg.get('artist', ''), mg.get('album', ''),
                                         mg.get('genre', ''), mg.get('date', '')))

    def play(self):
        self.stopped = False
        song = playlist_box.get(ACTIVE)
        song = f'/home/apoorv/Music/{song}.ogg'

        pygame.mixer.music.load(song)
        pygame.mixer.music.play()

        self.play_time()

        seek_bar.config(to=int(song_length), value=0, state=ACTIVE)

    def pause(self):
        if self.paused:
            pygame.mixer.music.unpause()
            self.paused = False
        elif not self.paused:
            pygame.mixer.music.pause()
            self.paused = True

    def next_track(self):
        seek_bar.config(value=0, state=ACTIVE)
        song_length_label.config(text='--:--/--:--')

        next_song = playlist_box.curselection()

        next_song = next_song[0]+1

        song = playlist_box.get(next_song)
        song = f'/home/apoorv/Music/{song}.ogg'

        pygame.mixer.music.load(song)
        pygame.mixer.music.play()

        playlist_box.selection_clear(0, END)
        playlist_box.activate(next_song)
        playlist_box.selection_set(next_song, last=None)

        seek_bar.config(value=0)

        print(next_song, song)

    def prev_track(self):
        seek_bar.config(value=0, state=ACTIVE)
        song_length_label.config(text='--:--/--:--')

        next_song = playlist_box.curselection()

        next_song = next_song[0]-1

        song = playlist_box.get(next_song)
        song = f'/home/apoorv/Music/{song}.ogg'

        pygame.mixer.music.load(song)
        pygame.mixer.music.play()

        playlist_box.selection_clear(0, END)
        playlist_box.activate(next_song)
        playlist_box.selection_set(next_song, last=None)

        seek_bar.config(value=0)

        print(next_song, song)

    def stop(self):
        seek_bar.config(value=0, state=DISABLED)
        song_length_label.config(text='--:--/--:--')

        pygame.mixer.music.stop()
        playlist_box.selection_clear(ACTIVE)

        self.stopped = True

    def show_hide_playlist(self):
        if self.playlist_is_hidden:
            self.root.geometry("320x500")
            self.playlist_frame()
            self.playlist_list_box()
#            self.playlist_tree_view()
            self.playlist_button()
            self.playlist_is_hidden = False
        elif not self.playlist_is_hidden:
            self.root.geometry("320x200")
#            for widget in playlist_frame_label.winfo_children():
#                widget.destroy()
#                print(widget)
#            playlist_box.grid_remove()
            playlist_frame_label.grid_forget()
#            playlist_box.grid_remove()
#            playlist_tree.destroy()
            self.playlist_is_hidden = True

    def show_hide_library(self):
        if self.library_is_hidden:
            self.playlist_tree_view()
            self.library_is_hidden = False
        elif not self.library_is_hidden:
            lib_win.destroy()
#            playlist_tree_frame.grid_remove()
            self.library_is_hidden = True

    def remove_track(self):
        playlist_box.delete(ANCHOR)
        pygame.mixer.music.stop()

    def clear_playlist(self):
        self.stop()
       
        playlist_box.delete(0, END)
        pygame.mixer.music.stop()

    def seek_bar(self, x):
#        pass
        song = playlist_box.get(ACTIVE)
        song = f'/home/apoorv/Music/{song}.ogg'

        pygame.mixer.music.load(song)
        pygame.mixer.music.play(start=seek_bar.get())


#        song = playlist_box.get(ACTIVE)
#        song = f'/home/apoorv/Music/{song}.ogg'
      
#        song_load = OggFileType(song)
#        song_length = song_load.info.length

#        song_length_info = time.strftime('%M:%S', time.gmtime(song_length))

#        volume_label.config(text=f'HELLO {int(seek.get())} of {int(song_length)}')
#        seek.config(to=int(song_length_info), value=0)

    def volume(self, x):
        pg_get_vol = pygame.mixer.music.get_volume() * 100

        pygame.mixer.music.set_volume(vol_slider.get())

        print(int(pg_get_vol))

    def volume_mute(self):
        pg_get_vol = pygame.mixer.music.get_volume() * 100
        if not self.muted:
            pygame.mixer.music.set_volume(0)
            print('muted: ' + str(pg_get_vol))
            self.muted = True
        elif self.muted:
            pygame.mixer.music.set_volume(100)
            print('unmuted: ' + str(pg_get_vol))
            self.muted = False

    def main_app(self):

        self.player_frame()
        self.player_button()
        self.seek_bar_scale()

#        self.volume_frame()
        self.volume_slider()
        self.volume_mute_button()

#        self.playlist_frame()
#        self.playlist_list_box()
#        self.playlist_tree_view()
#        self.playlist_button()

        self.status_bar()

        self.root.mainloop()

Event().main_app()
