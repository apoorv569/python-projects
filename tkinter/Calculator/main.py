from tkinter import *

root = Tk()
root.title("Calculator")

def num_button(next_num):
    current_num = input_num.get()
    input_num.delete(0, END)
    input_num.insert(0, str(current_num) + str(next_num))

def clear_button():
    input_num.delete(0, END)

def add_button():
    first_num = input_num.get()
    global num
    global op
    op = "add"
    num = int(first_num)
    input_num.delete(0, END)

def subtract_button():
    first_num = input_num.get()
    global num
    global op
    op = "sub"
    num = int(first_num)
    input_num.delete(0, END)

def multiply_button():
    first_num = input_num.get()
    global num
    global op
    op = "mul"
    num = int(first_num)
    input_num.delete(0, END)

def divide_button():
    first_num = input_num.get()
    global num
    global op
    op = "div"
    num = int(first_num)
    input_num.delete(0, END)

def period_button():
    pass

def equal_button():
    second_num = input_num.get()
    input_num.delete(0, END)

    if op == "add":
        input_num.insert(0, num + int(second_num))
    elif op == "sub":
        input_num.insert(0, num - int(second_num))
    elif op == "mul":
        input_num.insert(0, num * int(second_num))
    elif op == "div":
        input_num.insert(0, num / int(second_num))

input_num = Entry(root, width=40, borderwidth=5, justify=RIGHT, font="mononokinerdfont 10")
input_num.grid(row=0, column=0, columnspan=4, padx=20, pady=10, ipadx=20, ipady=20)

button1 = Button(root, text="1", padx=40, pady=20, command=lambda: num_button(1))
button2 = Button(root, text="2", padx=40, pady=20, command=lambda: num_button(2))
button3 = Button(root, text="3", padx=40, pady=20, command=lambda: num_button(3))
button4 = Button(root, text="4", padx=40, pady=20, command=lambda: num_button(4))
button5 = Button(root, text="5", padx=40, pady=20, command=lambda: num_button(5))
button6 = Button(root, text="6", padx=40, pady=20, command=lambda: num_button(6))
button7 = Button(root, text="7", padx=40, pady=20, command=lambda: num_button(7))
button8 = Button(root, text="8", padx=40, pady=20, command=lambda: num_button(8))
button9 = Button(root, text="9", padx=40, pady=20, command=lambda: num_button(9))
button0 = Button(root, text="0", padx=40, pady=20, command=lambda: num_button(0))

button_add = Button(root, text="+", padx=39, pady=20, command=add_button)
button_subtract = Button(root, text="-", padx=40, pady=20, command=subtract_button)
button_multiply = Button(root, text="*", padx=39, pady=20, command=multiply_button)
button_divide = Button(root, text="/", padx=40, pady=20, command=divide_button)

button_period = Button(root, text=".", padx=42, pady=20, command=period_button)

button_clear = Button(root, text="Clear", padx=170, pady=20, command=clear_button)
button_equal = Button(root, text="=", padx=40, pady=20, command=equal_button)

button1.grid(row=4, column=0)
button2.grid(row=4, column=1)
button3.grid(row=4, column=2)
button4.grid(row=3, column=0)
button5.grid(row=3, column=1)
button6.grid(row=3, column=2)
button7.grid(row=2, column=0)
button8.grid(row=2, column=1)
button9.grid(row=2, column=2)
button0.grid(row=5, column=0)

button_add.grid(row=5, column=3)
button_subtract.grid(row=4, column=3)
button_multiply.grid(row=3, column=3)
button_divide.grid(row=2, column=3)

button_period.grid(row=5, column=1)

button_clear.grid(row=1, column=0, columnspan=4)
button_equal.grid(row=5, column=2)


root.mainloop()
