import mutagen
from mutagen.oggvorbis import OggVorbis
from mutagen.wave import WAVE
import taglib
import wx

class DropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window
        self.main_list = []
#        self.log = log

    def OnDropFiles(self, x, y, filenames):
        print(filenames)

        for file_path in filenames:
            mg = mutagen.File(file_path, easy=True)
            sample_rate = WAVE(file_path).info.sample_rate
            bitrate = WAVE(file_path).info.bitrate
            length = WAVE(file_path).info.length
            channels = WAVE(file_path).info.channels
            bit_per_sample = WAVE(file_path).info.bits_per_sample
            wave_tags = WAVE(file_path).tags
            song = taglib.File(file_path)
            tags = song.tags

#            print(tags)
            self.main_list.append([False,
                                   song.tags['TITLE'],
                                   song.tags['ARTIST'],
                                   channels,
                                   length,
                                   sample_rate,
                                   bitrate,
                                   bit_per_sample])

            print(self.main_list)
            print("Sample rate: " + str(sample_rate))
            print("Bitrate: " + str(bitrate))
            print("Length: " + str(length))
            print("Channels: " + str(channels))
            print("Bits per sample: " + str(bit_per_sample))
#            print("Tags: " + str(wave_tags))
        return True
