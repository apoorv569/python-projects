import wx
from browser import Browser
from drop_target import DropTarget
from settings import Settings
from add_fav_dialog import AddFav

class MainFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, parent = None, title = "Sample Browser", size = (800,600))

#        self.drop_target = DropTarget(self)
        self.settings = Settings(self)
        self.add_fav = AddFav(self)
        self.browser = Browser(self, self.settings, self.add_fav)


app = wx.App()
frame = MainFrame()
frame.Show()
app.MainLoop()
