import os

class Utils:
    def __init__(self):
        pass

    def get_abspath(self, path):
        if os.name in ["posix", "darwin"]:
            path = os.path.expanduser(path)
        abspath = os.path.abspath(path)
        return os.path.realpath(abspath)

    def get_filename_no_extension(self, path):
        return os.path.splitext(os.path.basename(path))[0]
