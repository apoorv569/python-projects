import mutagen
from mutagen.oggvorbis import OggVorbis
from mutagen.wave import WAVE

import taglib

import wx
import wx.dataview
import wx.media

from database import Database
from utils import Utils
from widget_ids import BCID

class Browser(wx.Panel):
    def __init__(self, parent, settings_dialog, add_to_favorite_dialog):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition,
                          size=wx.DefaultSize, style=wx.TAB_TRAVERSAL)

        self.autoplay = False
        self.playing = False
        self.loop = False

        self.db = Database()
        self.utils = Utils()

#        self.drop_target = drop_target
        self.settings_dialog = settings_dialog
        self.add_to_favorite_dialog = add_to_favorite_dialog

        # Initializing BoxSizers
        TopSizer = wx.BoxSizer(wx.VERTICAL)

        BottomLeftPanelMainSizer = wx.BoxSizer(wx.VERTICAL)
        TopPanelMainSizer = wx.BoxSizer(wx.VERTICAL)
        BottomRightPanelMainSizer = wx.BoxSizer(wx.VERTICAL)

        SearchBoxSizer = wx.BoxSizer(wx.HORIZONTAL)
        ListCtrlSizer = wx.BoxSizer(wx.VERTICAL)

        BrowserControlSizer = wx.BoxSizer(wx.HORIZONTAL)
        WaveformDisplaySizer = wx.BoxSizer(wx.HORIZONTAL)

        # Creating top splitter window,
        # this will be used to drawing the player controls panel,
        # and another splitter that will hold a wxNotebook and wxListCtrl.
        TopSplitter = wx.SplitterWindow(self, wx.ID_ANY, wx.DefaultPosition,
                                        wx.DefaultSize, wx.SP_3D,
                                        name=("SPLIT TOP"))
        TopSplitter.SetMinimumPaneSize(10)
        TopSplitter.SetSashGravity(0.2)

        # Top half of top splitter window,
        # this panel will hold browser controls
        # and waveform display.
        TopPanel = wx.Panel(TopSplitter, wx.ID_ANY, wx.DefaultPosition,
                            wx.DefaultSize, wx.SP_3D, name=("SPLIT TOP RIGHT"))

        # Creating bottom half of the top splitter window,
        # which is another splitter window,
        # both its splits will hold a panel for showing
        # other widgets.
        BottomSplitter = wx.SplitterWindow(TopSplitter, wx.ID_ANY,
                                           wx.DefaultPosition, wx.DefaultSize,
                                           wx.SP_3D, name=("SPLIT BOTTOM"))
        BottomSplitter.SetMinimumPaneSize(10)
        BottomSplitter.SetSashGravity(0.2)

        # Left half of the bottom splitter window,
        # this panel will hold wxNotebook,
        # which will have 2 pages,
        # browse and collection.
        BottomLeftPanel = wx.Panel(BottomSplitter, wx.ID_ANY, wx.DefaultPosition,
                                   wx.DefaultSize, wx.TAB_TRAVERSAL,
                                   name=("MANAGER"))

        # Initializing wxNotebook
        ViewChoice = wx.Notebook(BottomLeftPanel, wx.ID_ANY, wx.DefaultPosition,
                                 wx.DefaultSize, 0, name=("NOTEBOOK"))

        # Initializing wxGenericDirCtrl as one of the wxNotebook page.
        self.DirCtrl = wx.GenericDirCtrl(ViewChoice,
                                    BCID.DIR_CTRL,
                                    wx.DirDialogDefaultFolderStr,
                                    wx.DefaultPosition,
                                    wx.DefaultSize,
                                    wx.DIRCTRL_3D_INTERNAL | wx.SUNKEN_BORDER,
                                    wx.EmptyString, 0)

        Path = self.DirCtrl.GetDefaultPath()
        print("Default path before: " + Path)

        self.DirCtrl.SetDefaultPath("/home")

        APath = self.DirCtrl.GetDefaultPath()
        print("Default path after: " + APath)

        # Initializing wxTreeCtrl as another page of wxNotebook
        self.CollectionView = wx.TreeCtrl(ViewChoice, BCID.COLLECTION_VIEW, wx.DefaultPosition,
                                     wx.DefaultSize, wx.TR_DEFAULT_STYLE)

        # Adding items to wxTreeCtrl
        self.favorites = self.CollectionView.AddRoot("Favorites")
#        kicks = self.CollectionView.AppendItem(favorites, "Kicks")
#        puncykick = self.CollectionView.AppendItem(kicks, "Puncy Kick")

        # Adding the pages to wxNotebook
        ViewChoice.AddPage(self.DirCtrl, "Browser", False)
        ViewChoice.AddPage(self.CollectionView, "Collection", False)

        # Right half of bottom splitter window,
        # this panel will hold wxDataViewListCtrl,
        # to show added samples.
        BottomRightPanel = wx.Panel(BottomSplitter, wx.ID_ANY,
                                    wx.DefaultPosition, wx.DefaultSize,
                                    wx.SP_3D, name=("SPLIT BOTTOM RIGHT"))

        # Setting which splitter window
        # should split in which direction
        TopSplitter.SplitHorizontally(TopPanel, BottomSplitter)
        BottomSplitter.SplitVertically(BottomLeftPanel, BottomRightPanel)

        # Initializing browser controls on top panel.
        self.AutoPlayCheck = wx.CheckBox(TopPanel, BCID.AUTOPLAY_CHECK, "Autoplay",
                                    wx.DefaultPosition, wx.DefaultSize,
                                    wx.CHK_2STATE)
        self.AutoPlayCheck.SetValue(False)
        self.VolumeSlider = wx.Slider(TopPanel, BCID.VOLUME_SLIDER, 100, 0, 100,
                                 wx.DefaultPosition, wx.DefaultSize,
                                 wx.SL_HORIZONTAL)
        Waveform = wx.Bitmap("/home/apoorv/Downloads/waveform.png",
                             wx.BITMAP_TYPE_PNG)
        WaveformViewer = wx.StaticBitmap(TopPanel, wx.ID_ANY, Waveform,
                                         wx.DefaultPosition, wx.DefaultSize)

        self.PlayButton = wx.Button(TopPanel, BCID.PLAY_BUTTON, "▶",
                                    wx.DefaultPosition, wx.DefaultSize, 0)
        self.LoopButton = wx.ToggleButton(TopPanel, BCID.LOOP_BUTTON, "🔄",
                                          wx.DefaultPosition, wx.DefaultSize, 0)
        self.SettingsButton = wx.Button(TopPanel, BCID.SETTINGS, "🔧",
                                        wx.DefaultPosition, wx.DefaultSize, 0)

        self.MediaCtrl = wx.media.MediaCtrl(self, BCID.MEDIA_CTRL, wx.EmptyString,
                                            wx.DefaultPosition, wx.DefaultSize,
                                            0, wx.EmptyString)

        # Initializing wxSearchCtrl on bottom right panel.
        self.SearchBox = wx.SearchCtrl(BottomRightPanel,
                                       wx.ID_ANY,
                                       "Search for samples..",
                                       wx.DefaultPosition,
                                       wx.DefaultSize,
                                       wx.TE_PROCESS_ENTER)
        self.SearchBox.ShowCancelButton(True)
#        self.SearchBox.ShowWithEffect(wx.SHOW_EFFECT_EXPAND, 5)

        # Initializing wxDataViewListCtrl on bottom right panel.
        self.SampleListView = wx.dataview.DataViewListCtrl(BottomRightPanel,
                                                           BCID.SAMPLE_LIST_VIEW,
                                                           wx.DefaultPosition,
                                                           wx.DefaultSize,
                                                           wx.dataview.DV_SINGLE |
                                                           wx.dataview.DV_HORIZ_RULES |
                                                           wx.dataview.DV_VERT_RULES)

#        self.SampleListViewModel = wx.dataview.DataViewModel()

#        self.SampleListView.AssociateModel(self.SampleListViewModel)

        # Adding columns to wxDataViewListCtrl.
        self.SampleListView.AppendToggleColumn("♥",
#                                               0,
                                               wx.dataview.DATAVIEW_CELL_ACTIVATABLE,
                                               30,
                                               wx.ALIGN_CENTER,
                                               wx.dataview.DATAVIEW_COL_RESIZABLE)
        self.SampleListView.AppendTextColumn("Filename",
#                                             1,
                                             wx.dataview.DATAVIEW_CELL_INERT,
                                             280,
                                             wx.ALIGN_LEFT,
                                             wx.dataview.DATAVIEW_COL_RESIZABLE |
                                             wx.dataview.DATAVIEW_COL_SORTABLE)
        self.SampleListView.AppendTextColumn("Sample Pack",
#                                             2,
                                             wx.dataview.DATAVIEW_CELL_INERT,
                                             140,
                                             wx.ALIGN_RIGHT,
                                             wx.dataview.DATAVIEW_COL_RESIZABLE |
                                             wx.dataview.DATAVIEW_COL_SORTABLE)
        self.SampleListView.AppendTextColumn("Channels",
#                                             3,
                                             wx.dataview.DATAVIEW_CELL_INERT,
                                             80,
                                             wx.ALIGN_RIGHT,
                                             wx.dataview.DATAVIEW_COL_RESIZABLE |
                                             wx.dataview.DATAVIEW_COL_SORTABLE)
        self.SampleListView.AppendTextColumn("Length",
#                                             4,
                                             wx.dataview.DATAVIEW_CELL_INERT,
                                             80,
                                             wx.ALIGN_RIGHT,
                                             wx.dataview.DATAVIEW_COL_RESIZABLE |
                                             wx.dataview.DATAVIEW_COL_SORTABLE)
        self.SampleListView.AppendTextColumn("Sample Rate",
#                                             5,
                                             wx.dataview.DATAVIEW_CELL_INERT,
                                             150,
                                             wx.ALIGN_RIGHT,
                                             wx.dataview.DATAVIEW_COL_RESIZABLE |
                                             wx.dataview.DATAVIEW_COL_SORTABLE)
        self.SampleListView.AppendTextColumn("Bitrate",
#                                             6,
                                             wx.dataview.DATAVIEW_CELL_INERT,
                                             100,
                                             wx.ALIGN_RIGHT,
                                             wx.dataview.DATAVIEW_COL_RESIZABLE |
                                             wx.dataview.DATAVIEW_COL_SORTABLE)
        self.SampleListView.AppendTextColumn("Bits per sample",
#                                             7,
                                             wx.dataview.DATAVIEW_CELL_INERT,
                                             70,
                                             wx.ALIGN_RIGHT,
                                             wx.dataview.DATAVIEW_COL_RESIZABLE |
                                             wx.dataview.DATAVIEW_COL_SORTABLE)

#        self.SampleListView.SetColumnWidth(0, 240)
#        self.SampleListView.SetColumnWidth(2, 160)
#        self.SampleListView.SetColumnWidth(1, 140)
#        self.SampleListView.SetColumnWidth(3, 100)
#        self.SampleListView.SetColumnWidth(4, 150)
#        self.SampleListView.SetColumnWidth(5, 70)
#        self.SampleListView.SetColumnWidth(6, 80)

#        self.SampleListView.EnableDropTarget()

#        self.SampleListView.SetDropTarget(self.drop_target)

#        Sdata = [[False, "two", "three", "four", "five", "six", "seven", "eight"],
#                 [False, "two", "three", "four", "five", "six", "seven", "eight"],
#                 [False, "two", "three", "four", "five", "six", "seven", "eight"],
#                 [False, "two", "three", "four", "five", "six", "seven", "eight"],
#                 [False, "two", "three", "four", "five", "six", "seven", "eight"],
#                 [False, "two", "three", "four", "five", "six", "seven", "eight"],
#                 [False, "two", "three", "four", "five", "six", "seven", "eight"]]

#        self.SampleListView.Connect(wx.EVT_DROP_FILES, wx.DropFilesEventHandler(AddSamples), None, self)

#        fDrop = wx.FileDropTarget
#        self.SampleListView.SetDropTarget(fDrop)

        # Binding events.
        self.Bind(wx.EVT_DIRCTRL_FILEACTIVATED, self.OnClickDirCtrl,
                  self.DirCtrl, BCID.DIR_CTRL)
        self.Bind(wx.EVT_BUTTON, self.OnClickPlay,
                  self.PlayButton, BCID.PLAY_BUTTON)
        self.Bind(wx.EVT_TOGGLEBUTTON, self.OnClickLoop,
                  self.LoopButton, BCID.LOOP_BUTTON)
        self.Bind(wx.media.EVT_MEDIA_FINISHED, self.OnClickLoop,
                  self.LoopButton, BCID.LOOP_BUTTON)
        self.Bind(wx.EVT_BUTTON, self.OnClickSettings,
                  self.SettingsButton, BCID.SETTINGS)
        self.Bind(wx.EVT_CHECKBOX, self.OnCheckAutoplay,
                  self.AutoPlayCheck, BCID.AUTOPLAY_CHECK)
        self.Bind(wx.EVT_SCROLL_THUMBTRACK, self.OnSlideVolume,
                  self.VolumeSlider, BCID.VOLUME_SLIDER)
        self.Bind(wx.dataview.EVT_DATAVIEW_SELECTION_CHANGED,
                  self.OnClickSampleView, self.SampleListView,
                  BCID.SAMPLE_LIST_VIEW)
        self.Bind(wx.dataview.EVT_DATAVIEW_ITEM_VALUE_CHANGED,
                  self.OnCheckFavorite, self.SampleListView,
                  BCID.SAMPLE_LIST_VIEW)
        self.Bind(wx.EVT_TEXT_ENTER, self.OnDoSearch,
                  self.SearchBox, BCID.SEARCH_BOX)
        self.Bind(wx.EVT_SEARCHCTRL_SEARCH_BTN, self.OnDoSearch,
                  self.SearchBox, BCID.SEARCH_BOX)
        self.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, self.OnCancelSearch,
                  self.SearchBox, BCID.SEARCH_BOX)

#        Bind(wxEVT_DIRCTRL_FILEACTIVATED, &Browser::AddSamples, this, self.DirCtrl.GetId())
#        self.DirCtrl.Connect(self.DirCtrl.GetId(), wxEVT_TREE_BEGIN_DRAG, wxTreeEventHandler(Browser::AddSamples), NULL, this)

        # Adding widgets to their sizers,
        # so they fit and scale according to them.
        TopSizer.Add(TopSplitter, 1, wx.ALL | wx.EXPAND, 2)

        BrowserControlSizer.Add(self.PlayButton, 1, wx.ALL |
                                wx.ALIGN_CENTER_HORIZONTAL |
                                wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 2)
        BrowserControlSizer.Add(self.LoopButton, 1, wx.ALL |
                                wx.ALIGN_CENTER_HORIZONTAL |
                                wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 2)
        BrowserControlSizer.Add(self.SettingsButton, 1, wx.ALL |
                                wx.ALIGN_CENTER_HORIZONTAL |
                                wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 2)
        BrowserControlSizer.Add(0,0,5, wx.ALL | wx.EXPAND, 2)
        BrowserControlSizer.Add(self.VolumeSlider, 1, wx.ALL |
                                wx.ALIGN_CENTER_HORIZONTAL |
                                wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 2)
        BrowserControlSizer.Add(self.AutoPlayCheck, 1, wx.ALL |
                                wx.ALIGN_CENTER_HORIZONTAL |
                                wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 2)

        WaveformDisplaySizer.Add(WaveformViewer, 1, wx.ALL |
                                 wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 2)
        TopPanelMainSizer.Add(WaveformDisplaySizer, 5, wx.ALL | wx.EXPAND, 2)

        TopPanelMainSizer.Add(BrowserControlSizer, 1, wx.ALL | wx.EXPAND, 2)

        BottomLeftPanelMainSizer.Add(ViewChoice, 1, wx.ALL |
                                     wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 2)

        SearchBoxSizer.Add(self.SearchBox, 1, wx.ALL |
                           wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 2)
        ListCtrlSizer.Add(self.SampleListView, 1, wx.ALL |
                          wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 2)

        BottomRightPanelMainSizer.Add(SearchBoxSizer, 1, wx.ALL | wx.EXPAND, 2)
        BottomRightPanelMainSizer.Add(ListCtrlSizer, 10, wx.ALL | wx.EXPAND, 2)

        self.SetSizer(TopSizer)
        TopSizer.Fit(self)
        TopSizer.SetSizeHints(self)
        TopSizer.Layout()

        # Setting which sizer the browser controls and waveform view panel should use,
        # and adjusting some properties of the sizer,
        # so it scales properly.
        TopPanel.SetSizer(TopPanelMainSizer)
        TopPanelMainSizer.Fit(TopPanel)
        TopPanelMainSizer.Layout()
        TopPanelMainSizer.SetSizeHints(TopPanel)

        # Setting which sizer the ViewChoice panel should use,
        # and adjusting some properties of the sizer,
        # so it scales properly.
        BottomLeftPanel.SetSizer(BottomLeftPanelMainSizer)
        BottomLeftPanelMainSizer.Fit(BottomLeftPanel)
        BottomLeftPanelMainSizer.SetSizeHints(BottomLeftPanel)
        BottomLeftPanelMainSizer.Layout()

        # Setting which sizer the ListCtrl and self.SearchBox panel should use,
        # and adjusting some properties of the sizer,
        # so it scales properly.
        BottomRightPanel.SetSizer(BottomRightPanelMainSizer)
        BottomRightPanelMainSizer.Fit(BottomRightPanel)
        BottomRightPanelMainSizer.SetSizeHints(BottomRightPanel)
        BottomRightPanelMainSizer.Layout()

    def OnClickDirCtrl(self, event):
        print("Clicked")

        path_list = []

        dirctrl_path = self.DirCtrl.GetFilePath()

        absolute_path = self.utils.get_abspath(dirctrl_path)
        filename = self.utils.get_filename_no_extension(absolute_path)

        path_list.append(dirctrl_path)

        print(path_list)
        print("dirctrl_path: " + dirctrl_path)
        print("absolute_path: " + absolute_path)
        print("filename: " + filename)

        main_list = []

        for file_path in path_list:
            mg = mutagen.File(file_path, easy=True)
            sample_rate = str(WAVE(file_path).info.sample_rate)
            bitrate = str(WAVE(file_path).info.bitrate)
            lengthf = (WAVE(file_path).info.length*10)
            lengthr = round(lengthf, 2)
            length = str(lengthr)
            channels = str(WAVE(file_path).info.channels)
            bit_per_sample = str(WAVE(file_path).info.bits_per_sample)
            wave_tags = WAVE(file_path).tags
            song = taglib.File(file_path)
            artist = song.tags.get('ARTIST', [''])[0]
            tags = song.tags

#            print(tags)
            main_list.append([False,
                              filename,
                              artist,
                              channels,
                              length,
                              sample_rate,
                              bitrate,
                              bit_per_sample])

            print(main_list)
            print("Sample rate: " + str(sample_rate))
            print("Bitrate: " + str(bitrate))
            print("Length: " + str(length))
            print("Channels: " + str(channels))
            print("Bits per sample: " + str(bit_per_sample))
#            print("Tags: " + str(wave_tags))

        self.db.insert_sample(0, filename, artist, channels, length,
                              sample_rate, bitrate, bit_per_sample,
                              absolute_path)
        spf = self.db.get_sample_path_by_filename(filename)
#        self.db.connection.close()

        print(spf)

        for item in main_list:
            self.SampleListView.AppendItem(item)

        print("<<< WE ARE IN A DIFFERENT CLASS NOW >>>")
        print(main_list)
#        print("\n Play: " + BCID.PLAY_BUTTON)
#        print("\n Settings: " + BCID.SETTINGS)

    def OnClickPlay(self, event):
        selection = self.SampleListView.GetTextValue(self.SampleListView.GetSelectedRow(), 1)
        sample = self.db.get_sample_path_by_filename(selection)[0]

        print("Selected row: " + selection)
        print("Sample: " + sample)

        self.MediaCtrl.Load(sample);
        self.MediaCtrl.Play();

    def OnClickLoop(self, event):
        if self.LoopButton.GetValue():
            print("Looping..")
            if self.MediaCtrl.GetState() == 0:
                print("Stopped")
            elif self.MediaCtrl.GetState() == 2:
                print("Playing..")
        else:
            print("Not looping")

    def OnClickSettings(self, event):
        self.settings_dialog.Show()

        if self.settings_dialog.ShowModal() == wx.ID_OK:
            pass
        elif self.settings_dialog.ShowModal() == wx.ID_APPLY:
            pass
        elif self.settings_dialog.ShowModal() == wx.ID_CANCEL:
            pass

    def OnClickSampleView(self, event):
        selection = self.SampleListView.GetTextValue(self.SampleListView.GetSelectedRow(), 1)
        sample = self.db.get_sample_path_by_filename(selection)[0]

        self.MediaCtrl.Load(sample)

        print(selection)
        print(sample)

        if self.autoplay:
            self.MediaCtrl.Play()
        else:
            pass

    def OnCheckFavorite(self, event):
        selection = self.SampleListView.GetTextValue(self.SampleListView.GetSelectedRow(), 1)
        child, cookie = self.CollectionView.GetFirstChild(self.favorites)
        children = self.CollectionView.GetNextChild(self.favorites, cookie)

        print(self.CollectionView.GetChildrenCount(self.favorites))

        if self.SampleListView.GetToggleValue(self.SampleListView.GetSelectedRow(), 0):

            for _ in range(1):
                if not selection in self.CollectionView.GetItemText(self.favorites):
                    print(self.CollectionView.GetItemText(self.favorites))
                    add = self.CollectionView.AppendItem(self.favorites, selection)
                    print(self.CollectionView.GetRootItem())
                elif selection in self.CollectionView.GetItemText(children[0]):
                    print("Already added.")

#            self.add_to_favorite_dialog.Show()
#            if self.add_to_favorite_dialog.ShowModal() == wx.ID_OK:
#                pass

        elif not self.SampleListView.GetToggleValue(self.SampleListView.GetSelectedRow(), 0):

            for _ in range(1):
                if selection in self.CollectionView.GetItemText(children[0]):
                    remove = self.CollectionView.Delete(children[0])

    def OnCheckAutoplay(self, event):
        if self.AutoPlayCheck.GetValue():
            self.autoplay = True
            print(self.AutoPlayCheck.GetValue())
        elif not self.AutoPlayCheck.GetValue():
            self.autoplay = False
            print(self.AutoPlayCheck.GetValue())

    def OnSlideVolume(self, event):
        getVolume = self.MediaCtrl.GetVolume() * 100.0

        print("wxMediaCtrl Vol: " + str(getVolume))
        print("Slider Vol: " + str(self.VolumeSlider.GetValue()))

        self.MediaCtrl.SetVolume(float(self.VolumeSlider.GetValue()) / 100)

    def OnDoSearch(self, event):
        search = self.SearchBox.GetValue()

        choices = ["Kick", "Snare", "HiHat", "Clap", "Shaker"]

        auto = self.SearchBox.AutoComplete(choices)
        print(auto)

    def OnCancelSearch(self, event):
        self.SearchBox.Clear()
