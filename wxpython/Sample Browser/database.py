import sqlite3

class Database:
    def __init__(self):
        self.connection = sqlite3.connect(':memory:')
        self.cursor = self.connection.cursor()

        self.cursor.execute("""CREATE TABLE Samples (
                                Fav integer,
                                Filename text,
                                SamplePack text,
                                Channels integer,
                                Length real,
                                SampleRate integer,
                                Bitrate integer,
                                BitsPerSample integer,
                                Path text)""")
        self.connection.commit()

#        self.connection.close()

    def insert_sample(self, fav, filename, samplepack, channels, length,
                      samplerate, bitrate, bitspersample, path):
        with self.connection:
            self.cursor.execute("""INSERT INTO Samples VALUES
                                (:f, :fn, :sp, :c, :l, :sr, :b, :bps, :p)""",
                                {'f': fav, 'fn': filename, 'sp': samplepack,
                                 'c': channels, 'l': length, 'sr': samplerate,
                                 'b': bitrate, 'bps': bitspersample, 'p': path})

    def get_sample_path_by_filename(self, filename):
        self.cursor.execute("""SELECT Path FROM Samples WHERE
                            filename = :filename""",
                            {'filename': filename})
        return self.cursor.fetchone()

    def update_data(self, data, value):
        with self.connection:
            self.cursor.execute("""UPDATE Samples SET data = :value""",
                                {'data': value})

    def remove_data(self, data):
        pass
