import wx
from widget_ids import BCID

class AddFav(wx.Dialog):
    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, BCID.COLLECTION_VIEW, "Add to favorites..",
                           wx.DefaultPosition, wx.DefaultSize,
                           wx.DEFAULT_DIALOG_STYLE | wx.STAY_ON_TOP)
