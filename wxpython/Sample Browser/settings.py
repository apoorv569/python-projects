import wx
from widget_ids import BCID

class Settings(wx.Dialog):
    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, BCID.SETTINGS, "Settings",
                           wx.DefaultPosition, wx.DefaultSize,
                           wx.DEFAULT_DIALOG_STYLE | wx.STAY_ON_TOP)

        MainSizer = wx.BoxSizer(wx.VERTICAL)
        MainChoiceSizer = wx.BoxSizer(wx.VERTICAL)
        Choice1Sizer = wx.BoxSizer(wx.HORIZONTAL)
        Choice2Sizer = wx.BoxSizer(wx.HORIZONTAL)
        RadioSizer = wx.BoxSizer(wx.HORIZONTAL)
        CheckSizer = wx.BoxSizer(wx.HORIZONTAL)
        ButtonSizer = wx.BoxSizer(wx.HORIZONTAL)

        Panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize)

        Choices = ["Option 1", "Option 2", "Option 3", "Option 4"]

        Label = wx.StaticText(Panel, wx.ID_ANY, "Choose one", wx.DefaultPosition, wx.DefaultSize)
        Label1 = wx.StaticText(Panel, wx.ID_ANY, "Choose two", wx.DefaultPosition, wx.DefaultSize)
        Choice = wx.Choice(Panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, Choices)
        Choice1 = wx.Choice(Panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, Choices)
        Radio = wx.RadioButton(Panel, wx.ID_ANY, "Radio", wx.DefaultPosition, wx.DefaultSize)
        Radio1 = wx.RadioButton(Panel, wx.ID_ANY, "Radio", wx.DefaultPosition, wx.DefaultSize)
        Check = wx.CheckBox(Panel, wx.ID_ANY, "Check", wx.DefaultPosition, wx.DefaultSize)

        Button = wx.Button(Panel, wx.ID_OK, "OK", wx.DefaultPosition, wx.DefaultSize)
        Button1 = wx.Button(Panel, wx.ID_APPLY, "Apply", wx.DefaultPosition, wx.DefaultSize)
        Button2 = wx.Button(Panel, wx.ID_CANCEL, "Cancel", wx.DefaultPosition, wx.DefaultSize)

        Choice1Sizer.Add(Label, 1, wx.ALL | wx.EXPAND, 2)
        Choice1Sizer.Add(Choice, 3, wx.ALL | wx.EXPAND, 2)
        Choice2Sizer.Add(Label1, 1, wx.ALL | wx.EXPAND, 2)
        Choice2Sizer.Add(Choice1, 3, wx.ALL | wx.EXPAND, 2)

        MainChoiceSizer.Add(Choice1Sizer, 1, wx.ALL | wx.EXPAND, 2)
        MainChoiceSizer.Add(Choice2Sizer, 1, wx.ALL | wx.EXPAND, 2)

        RadioSizer.Add(Radio, 1, wx.ALL | wx.EXPAND, 2)
        RadioSizer.Add(Radio1, 1, wx.ALL | wx.EXPAND, 2)

        CheckSizer.Add(Check, 1, wx.ALL | wx.EXPAND, 2)

        ButtonSizer.Add(0, 0, 2, wx.ALL | wx.EXPAND, 2)
        ButtonSizer.Add(Button, 1, wx.ALL | wx.EXPAND, 2)
        ButtonSizer.Add(Button1, 1, wx.ALL | wx.EXPAND, 2)
        ButtonSizer.Add(Button2, 1, wx.ALL | wx.EXPAND, 2)

        MainSizer.Add(MainChoiceSizer, 1, wx.ALL | wx.EXPAND, 2)
        MainSizer.Add(RadioSizer, 1, wx.ALL | wx.EXPAND, 2)
        MainSizer.Add(CheckSizer, 1, wx.ALL | wx.EXPAND, 2)
        MainSizer.Add(ButtonSizer, 1, wx.ALL | wx.EXPAND, 2)

        Panel.SetSizer(MainSizer)
        MainSizer.Fit(Panel)
        MainSizer.SetSizeHints(Panel)
        MainSizer.Layout()
