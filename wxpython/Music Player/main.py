import wx
from Modules.gui import GUI
from Modules.command import Command

class MyFrame(wx.Frame):
    """ We simply derive a new class of Frame. """
    def __init__(self, parent, title, size):
        wx.Frame.__init__(self, parent, title=title, size=size)    # size 350, 200
        self.Show(True)
        self.panel = wx.Panel(self)
        self.gui = GUI(self, self.panel)
        self.command = Command(self, self.panel, self.gui)

app = wx.App(False)
frame = MyFrame(None, "Linamp - Whoop the tux's ass!", (500, 500))
app.MainLoop()
