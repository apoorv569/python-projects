correct_words = ["linux", "gnu", "foss", "oss"]
banned_words = ["microsoft", "proprietary", "windows"]
guess_count = 0
guess_limit = 3

while True:
    if guess_count == guess_limit:
        print("You Lose! Better luck next time.")
        break

    guess = input("Guess the correct word: ")
    guess_count += 1
    is_good = guess in correct_words
    is_bad = guess in banned_words

    if is_good:
        print("You Win! YAY.")
        break
    elif is_bad:
        print("You Lose! Better luck next time.")
        break
    elif guess_count > guess_limit:
        print("Out of tries. You Lose!")
        break

