def get_num(ques="Enter a number"):
    tries = 3
    count = 0
    while True:
        try:
            count += 1
            return float(input(ques))
        except Exception:
            if count < tries:
                print(f"Error! {n} is not a valid number.")
            else:
                print("Error! Program Terminated!!")
                exit()

def calculator():
    num1 = get_num("Enter First Number: ")
    num2 = get_num("Enter Second Number: ")

    valid_operations = ["+", "add", "-", "sub", "*", "mul", "/", "div"]

    while True:
        op = input("Do you want to add, sub, mul or div (" + ', '.join(valid_operations) + ")?:")
        if op in valid_operations:
            break
        else:
            print(f"Error! {op} is not a valid operation. Please try again.")

    if op == "+":
        print(num1 + num2)
    elif op == "-":
        print(num1 - num2)
    elif op == "*":
        print(num1 * num2)
    elif op == "/":
        print(num1 / num2)

while True:
    calculator()
    if input("Type Exit to exit or press any key to continue.") == "Exit":
        break
