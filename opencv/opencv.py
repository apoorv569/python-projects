import cv2 as cv

# Read image file
img = cv.imread('lena.jpg')

def rescaleFrame(frame, scale=0.75):
    """Rescale the image/video"""
    width = int(frame.shape[1] * scale)
    height = int(frame.shape[0] * scale)
    dimensions = (width, height)
    return cv.resize(frame, dimensions, interpolation=cv.INTER_AREA)

def changeRes(width, height):
    """Change resolution of video, only works for live feed"""
    capture.set(3, width)
    capture.set(4, height)

# Read video feed or files.
capture = cv.VideoCapture(0)

while True:
    isTrue, frame = capture.read()

#    frame_resized = rescaleFrame(frame)
    frame_resized = changeRes(800, 600)

    cv.imshow('Video', frame)
#    cv.imshow('Video Resized', frame_resized)

    if cv.waitKey(20) & 0xFF==ord('d'):
        break

capture.release()
cv.destroyAllWindows()
